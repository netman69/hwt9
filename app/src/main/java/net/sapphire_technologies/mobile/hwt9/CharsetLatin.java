package net.sapphire_technologies.mobile.hwt9;

/**
 * Created by netman on 1/04/2017.
 *
 * Latin charset
 */
public class CharsetLatin extends Charset {
    @Override
    public String getName() {
        return "latin";
    }

    @Override
    public String getVisibleName() {
        return "Latin";
    }

    @Override
    public String getAbc() {
        return "abc";
    }

    @Override
    public char[] getMap(char n) {
        switch (n) {
            case 1:
                return new char[] { '.', ',', '?', '!', '\'', '"', '1', '-', '(', ')', '@', '/', ':', ';', '_', '*', '|' };
            case 2:
                return new char[] { 'a', 'b', 'c', '2', 'ä', 'à', 'á', 'â', 'ã', 'å', 'æ', 'ç' };
            case 3:
                return new char[] { 'd', 'e', 'f', '3', 'é', 'è', 'ë', 'ê', 'đ' };
            case 4:
                return new char[] { 'g', 'h', 'i', '4', 'î', 'ï', 'ì', 'í' };
            case 5:
                return new char[] { 'j', 'k', 'l', '5', '£' };
            case 6:
                return new char[] { 'm', 'n', 'o', '6', 'ö', 'ô', 'ò', 'ó', 'õ', 'ø', 'ñ' };
            case 7:
                return new char[] { 'p', 'q', 'r', 's', '7', 'ß', '$' };
            case 8:
                return new char[] { 't', 'u', 'v', '8', 'ü', 'ù', 'ȗ', 'ú' };
            case 9:
                return new char[] { 'w', 'x', 'y', 'z', '9', 'ý' };
            case 0:
                return new char[] { ' ', '0' };
        }
        return null;
    }

    @Override
    public char getMapReverse(char c) {
        switch (c) {
            case '.':
            case ',':
            case '?':
            case '!':
            case '\'':
            case '"':
            case '1':
            case '-':
            case '(':
            case ')':
            case '@':
            case '/':
            case ':':
            case ';':
            case '_':
            case '*':
            case '|':
                return 1;
            case 'a':
            case 'b':
            case 'c':
            case '2':
            case 'ä':
            case 'à':
            case 'á':
            case 'â':
            case 'ã':
            case 'å':
            case 'æ':
            case 'ç':
                return 2;
            case 'd':
            case 'e':
            case 'f':
            case '3':
            case 'é':
            case 'è':
            case 'ë':
            case 'ê':
            case 'đ':
                return 3;
            case 'g':
            case 'h':
            case 'i':
            case '4':
            case 'î':
            case 'ï':
            case 'ì':
            case 'í':
                return 4;
            case 'j':
            case 'k':
            case 'l':
            case '5':
            case '£':
                return 5;
            case 'm':
            case 'n':
            case 'o':
            case '6':
            case 'ö':
            case 'ô':
            case 'ò':
            case 'ó':
            case 'õ':
            case 'ø':
            case 'ñ':
                return 6;
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case '7':
            case 'ß':
            case '$':
                return 7;
            case 't':
            case 'u':
            case 'v':
            case '8':
            case 'ü':
            case 'ù':
            case 'ȗ':
            case 'ú':
                return 8;
            case 'w':
            case 'x':
            case 'y':
            case 'z':
            case '9':
            case 'ý':
                return 9;
            case ' ':
            case '0':
                return 0;
        }
        return 10;
    }

    @Override
    public char[] getSpecial() {
        return new char[] { '.', ',', '\'', '?', '!', '\"', '-', '(', ')', '@', '/', ':', '_', ';', '+', '&', '%', '*', '=', '<', '>', '£', '€', '$', '¥', '°', '[', ']', '{', '}', '\\', '~', '^', '¡', '¿', '§', '#', '|', 'μ', '\n' };
    }

    @Override
    public boolean isWordCharacter(char c) {
        int n = getMapReverse(Character.toLowerCase(c));
        return ((n > 1 && n < 10 && (c > '9' || c < '0')) || c == '\'');
    }

    @Override
    public boolean needCapital(String bc, int textMode) {
        if (bc != null && bc.length() > 2)
            bc = bc.substring(0, 2);
        if (textMode == TextMode.AUTO) {
            if (bc == null || bc.length() == 0 || bc.equals(". ") || bc.equals("! ") || bc.equals("? ") || bc.substring(bc.length() - 1).equals("\n"))
                return true;
        }
        return textMode == TextMode.UPPERCASE;
    }
}
