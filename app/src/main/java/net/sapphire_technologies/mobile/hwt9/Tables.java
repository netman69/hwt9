package net.sapphire_technologies.mobile.hwt9;

import android.view.KeyEvent;

/**
 * Created by netman on 3/19/17.
 *
 * Character set
 */
class Tables {
    public static char getKCNum(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
                return 1;
            case KeyEvent.KEYCODE_2:
                return 2;
            case KeyEvent.KEYCODE_3:
                return 3;
            case KeyEvent.KEYCODE_4:
                return 4;
            case KeyEvent.KEYCODE_5:
                return 5;
            case KeyEvent.KEYCODE_6:
                return 6;
            case KeyEvent.KEYCODE_7:
                return 7;
            case KeyEvent.KEYCODE_8:
                return 8;
            case KeyEvent.KEYCODE_9:
                return 9;
            case KeyEvent.KEYCODE_0:
                return 0;
        }
        return 10;
    }

// --Commented out by Inspection START (28/03/2017 10:54):
//    public static int getNumKC(int n) {
//        switch (n) {
//            case 1:
//                return KeyEvent.KEYCODE_1;
//            case 2:
//                return KeyEvent.KEYCODE_2;
//            case 3:
//                return KeyEvent.KEYCODE_2;
//            case 4:
//                return KeyEvent.KEYCODE_4;
//            case 5:
//                return KeyEvent.KEYCODE_5;
//            case 6:
//                return KeyEvent.KEYCODE_6;
//            case 7:
//                return KeyEvent.KEYCODE_7;
//            case 8:
//                return KeyEvent.KEYCODE_8;
//            case 9:
//                return KeyEvent.KEYCODE_9;
//            case 0:
//                return KeyEvent.KEYCODE_0;
//        }
//        return KeyEvent.KEYCODE_UNKNOWN;
//    }
// --Commented out by Inspection STOP (28/03/2017 10:54)
}
