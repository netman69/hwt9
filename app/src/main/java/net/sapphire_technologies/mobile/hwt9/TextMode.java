package net.sapphire_technologies.mobile.hwt9;

import android.graphics.Paint;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by netman on 27/03/2017.
 *
 * Input mode switching magic
 */
@SuppressWarnings("deprecation")
public class TextMode {
    public final static int AUTO = 0;
    public final static int LOWERCASE = 1;
    public final static int UPPERCASE = 2;
    public final static int NUMBERS = 3;

    public int mode = TextMode.AUTO;
    public boolean isPredictive = true;
    public Charset charset = Charsets.getAll()[0];

    private boolean isTimedOut = true;
    private int switchCount = 0;
    private boolean reCapital = false;
    private boolean reCapitalRotate = false;
    private String textModeName;
    private boolean predictiveEnabled = true;
    private int charsetIdx = 0;
    private final HWT9 hwt9;
    private final HWT9.InputContext context;

    private final Runnable callback = new Runnable() {
        @Override
        public void run() {
            isTimedOut = true;
            updateView();
        }
    };

    TextMode(HWT9 hwt9, HWT9.InputContext context) {
        this.hwt9 = hwt9;
        this.context = context;
        charset = hwt9.charsets.get(charsetIdx);
        setTextMode(mode, isPredictive, reCapital, charset);
    }

    public void setCharset(Charset cs) {
        setTextMode(mode, isPredictive, reCapital, cs);
    }

    public void setLanguage(Language language) {
        setTextMode(mode, isPredictive, reCapital, language.charset);
    }

    @SuppressWarnings("SameParameterValue")
    public void setPredictiveEnabled(boolean enabled) {
        predictiveEnabled = enabled;
        if (!predictiveEnabled)
            isPredictive = false;
        setTextMode(mode, isPredictive, reCapital, charset);
    }

    public void rotate() {
        /* Press 3 times fast to switch between from predictive to normal */
        /* Press 3 times fast to switch between from normal to numbers */
        /* Press once for numbers to predictive */
        /* Above was written before reCapital stuff so it's only partly correct */
        /* In auto mode pressing # when there shouldn't be an automatic capital will color the Abc
             and make a capital letter once before commencing to rotate */

        /* Set up timer */
        boolean isTimedOut = this.isTimedOut;
        this.isTimedOut = false; /* setTextMode() will call updateView() */
        hwt9.handler.removeCallbacks(callback);
        hwt9.handler.postDelayed(callback, hwt9.prefs.textModeSwitchTime);
        if (isTimedOut)
            switchCount = 0;

        /* Figure out if we need to rotate repeat capital */
        if (switchCount == 0)
            reCapitalRotate = canReCapital();
        int nrot = (reCapitalRotate ? 4 : 3);

        /* Deal with mode change */
        ++switchCount;
        if (!isTimedOut && isPredictive && switchCount >= nrot) {
            setTextMode(TextMode.AUTO, false, false, hwt9.charsets.get(0));
            switchCount = 0;
            return;
        } else if (!isTimedOut && !isPredictive && switchCount >= nrot) {
            if (!rotateCharset()) {
                setTextMode(TextMode.NUMBERS, false, false, charset);
                switchCount = 0;
            } else switchCount = 0;
            return;
        }
        switch (mode) {
            case AUTO:
                /* Handle repeated capitals stuff */
                if (reCapital) {
                    reCapital = false;
                    setTextMode(TextMode.LOWERCASE, isPredictive, false, charset);
                    break;
                } else if (reCapitalRotate) {
                    setTextMode(TextMode.AUTO, isPredictive, true, charset);
                    break;
                }
                /* Handle normal rotation options */
                //noinspection ConstantConditions
                setTextMode(TextMode.LOWERCASE, isPredictive, reCapital, charset);
                break;
            case LOWERCASE:
                setTextMode(TextMode.UPPERCASE, isPredictive, reCapital, charset);
                break;
            case UPPERCASE:
                setTextMode(TextMode.AUTO, isPredictive, reCapital, charset);
                break;
            case NUMBERS:
                setTextMode(TextMode.AUTO, predictiveEnabled, reCapital, hwt9.charsets.get(0));
                switchCount = 0;
                break;
        }
    }

    public void updateView() {
        if (hwt9.kv != null && this == context.textMode) {
            TextView tv = (TextView) hwt9.kv.findViewById(R.id.keyMode);
            if (tv != null) {
                tv.setText(textModeName);
                tv.setTextColor(hwt9.getResources().getColor(isNextAutoCapital() ? R.color.active : R.color.foreground));
                if (isTimedOut)
                    tv.setPaintFlags(tv.getPaintFlags() & ~Paint.UNDERLINE_TEXT_FLAG);
                else tv.setPaintFlags(tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            }
            LinearLayout ll = (LinearLayout) hwt9.kv.findViewById(R.id.predictionsBar);
            if (ll != null) {
                if ((isPredictive/* && prefs.showPredictions */) || hwt9.addingWord)
                    ll.setVisibility(View.VISIBLE);
                else ll.setVisibility(View.GONE);
            }
        }
    }

    public void setTextMode(int tm, boolean pre, boolean re, Charset cs) {
        boolean isChanged = (tm != mode || pre != isPredictive || re != reCapital || !charset.equals(cs));
        int oldmode = mode;
        if (isChanged && pre != isPredictive || tm == TextMode.NUMBERS)
            hwt9.endInput(context);
        mode = tm;
        isPredictive = pre;
        reCapital = re;
        charset = cs;
        if (isPredictive && hwt9.languages != null)
            charset = hwt9.languages.current.charset;
        if (mode == TextMode.NUMBERS)
            charset = Charsets.getAll()[0];
        int idx = hwt9.charsets.indexOf(cs);
        if (idx != -1)
            charsetIdx = idx;
        textModeName = "";
        if (isPredictive)
            textModeName = "*";
        String abc = charset.getAbc();
        switch (mode) {
            case AUTO:
                textModeName += abc.substring(0, 1).toUpperCase() + abc.substring(1);
                break;
            case LOWERCASE:
                textModeName += abc;
                break;
            case UPPERCASE:
                textModeName += abc.toUpperCase();
                break;
            case NUMBERS:
                textModeName += "123";
                break;
        }
        if (isPredictive && oldmode != mode)
            context.predict.onTextModeChanged();
        updateView();
        if (hwt9.kv != null && !hwt9.kv.isShown() && isChanged)
            hwt9.showWindow(true);
    }

    public boolean autoCapital() {
        boolean isCapital;
        if (reCapital) {
            setTextMode(mode, isPredictive, false, charset);
            return true;
        }
        InputConnection ic = context.ic;
        CharSequence bc = ((ic != null) ? ic.getTextBeforeCursor(2, 0) : null);
        if (bc != null)
            isCapital = charset.needCapital(bc.toString(), mode);
        else isCapital = charset.needCapital(null, mode);
        return isCapital;
    }

    private boolean rotateCharset() {
        ++charsetIdx;
        if (charsetIdx >= hwt9.charsets.size()) {
            charsetIdx = 0;
            return false; /* This function returns false if gone all the way around */
        }
        setTextMode(mode, isPredictive, reCapital, hwt9.charsets.get(charsetIdx));
        return true;
    }

    private boolean canReCapital() {
        if (reCapital)
            return true;
        InputConnection ic = context.ic;
        CharSequence bc = ((ic != null) ? ic.getTextBeforeCursor(2, 0) : null);
        if (bc != null)
            return !charset.needCapital(bc.toString(), TextMode.AUTO);
        else return !charset.needCapital(null, TextMode.AUTO);
    }

    private boolean isNextAutoCapital() {
        if (mode != TextMode.AUTO)
            return false;
        if (reCapital)
            return true;
        InputConnection ic = context.ic;
        CharSequence bc = ((ic != null) ? ic.getTextBeforeCursor(2, 0) : null);
        if (bc != null)
            return charset.needCapital(bc.toString(), mode);
        else return charset.needCapital(null, mode);
    }
}
