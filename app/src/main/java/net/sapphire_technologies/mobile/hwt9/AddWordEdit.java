package net.sapphire_technologies.mobile.hwt9;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by netman on 28/03/2017.
 *
 * EditText with onSelectionChanged listener that always takes focus
 */
public class AddWordEdit extends EditText {
    public interface OnSelectionChangedListener {
        @SuppressWarnings("UnusedParameters")
        void onSelectionChanged(int selStart, int selEnd);
    }

    private OnSelectionChangedListener onSelectionChangedListener;

    public AddWordEdit(Context context){
        super(context);
    }

    public AddWordEdit(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public AddWordEdit(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        if (onSelectionChangedListener != null)
            onSelectionChangedListener.onSelectionChanged(selStart, selEnd);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (!focused)
            requestFocus();
    }

    public void setOnSelectionChangedListener(OnSelectionChangedListener listener) {
        onSelectionChangedListener = listener;
    }
}
