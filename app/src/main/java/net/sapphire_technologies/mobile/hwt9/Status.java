package net.sapphire_technologies.mobile.hwt9;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Stack;

/**
 * Created by netman on 27/03/2017.
 *
 * For the showing of status messages in our view
 */
public class Status {
    final private Stack<Item> items = new Stack<>();
    private final HWT9 hwt9;

    public static class Item {
        public String status;
        public boolean loading = false;
    }

    /* Thread-safe status displaying utility class */

    public Status(HWT9 hwt9) {
        this.hwt9 = hwt9;
    }

    public Item add(String status, boolean loading) {
        Item it = new Item();
        it.status = status;
        it.loading = loading;
        synchronized (items) {
            items.push(it);
        }
        updateView();
        return it;
    }

    @SuppressWarnings({"SameParameterValue", "UnusedReturnValue"})
    public Item addTimed(String status, boolean loading) {
        final Item it = add(status, loading);
        hwt9.handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                remove(it);
            }
        }, hwt9.prefs.tempStatusTime);
        return it;
    }

    public void remove(Item it) {
        synchronized (items) {
            items.remove(it);
        }
        updateView();
    }

    public synchronized void updateView() {
        final Item it;
        synchronized (items) {
            it = ((items.size() > 0) ? items.lastElement() : null);
        }
        hwt9.handler.post(new Runnable() {
            @Override
            public void run() {
                if (hwt9.kv != null) {
                    TextView status = (TextView) hwt9.kv.findViewById(R.id.status);
                    status.setText((it != null) ? it.status : hwt9.getString(R.string.status_noStatus));
                    ProgressBar pb = (ProgressBar) hwt9.kv.findViewById(R.id.statusLoading);
                    if (it != null && it.loading)
                        pb.setVisibility(View.VISIBLE);
                    else pb.setVisibility(View.GONE);
                }
            }
        });
    }
}
