package net.sapphire_technologies.mobile.hwt9;

import android.content.res.AssetManager;
import android.os.Process;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by netman on 28/03/2017.
 *
 * Language class
 */
class Language {
    public final String name;
    public Charset charset;
    public Trie trie;

    private final HWT9 hwt9;
    private final String fileName;
    private final boolean isAddon;
    private boolean enabled = false;
    private Thread thread;
    public final UserDictionary userDictionary;

    public Language(HWT9 hwt9, String name, String fileName, boolean isAddon) {
        this.hwt9 = hwt9;
        this.name = name;
        this.fileName = fileName;
        this.isAddon = isAddon;
        userDictionary = new UserDictionary(hwt9, fileName);
    }

    public boolean load() {
        final INIParser parser;
        InputStream stream;
        if (!isAddon)
            userDictionary.load();
        try {
            stream = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            try {
                AssetManager am = hwt9.getAssets();
                stream = am.open(Languages.assetsdir + "/" + fileName);
            } catch (Exception e2) {
                return false;
            }
        }
        parser = new INIParser(stream, true);
        try {
            if (!parser.hasData) throw new Exception(String.format(hwt9.getString(R.string.status_invalidDictionary), fileName));
            HashMap<String, String> info;
            if (isAddon)
                info = parser.get("addondictionary");
            else info = parser.get("dictionary");
            if (info == null) throw new Exception(String.format(hwt9.getString(R.string.status_invalidDictionary), fileName));
            String csname = info.get("charset");
            if (csname == null) throw new Exception(String.format(hwt9.getString(R.string.status_invalidDictionary), fileName));
            charset = Charsets.get(csname);
            if (charset == null) throw new Exception(String.format(hwt9.getString(R.string.status_invalidDictionary), fileName));
        } catch (Exception e) {
            hwt9.status.addTimed(e.getMessage(), false);
            Log.d("HWT9 Languages", e.getMessage());
            return false;
        }
        initTrie();
        final Status.Item it = hwt9.status.add(hwt9.getString(R.string.status_loadingDictionary), true);
        enabled = true;
        thread = new Thread() {
            @Override
            public void run() {
                String line;
                while ((line = parser.getLine()) != null) {
                    trie.add(charset, line, false);
                    synchronized (thread) {
                        if (!enabled)
                            break;
                    }
                }
                parser.end();
                hwt9.status.remove(it);
            }
        };
        thread.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        return true;
    }

    public void unLoad() {
        if (thread != null) {
            synchronized (thread) {
                enabled = false;
            }
        }
        userDictionary.unLoad();
        initTrie();
        /*Runtime.getRuntime().gc();
        System.gc();*/
    }

    private void initTrie() {
        trie = new Trie(hwt9.prefs);
        if (!isAddon) {
            char[] key1 = charset.getMap((char) 1);
            //noinspection ConstantConditions
            for (char aKey1 : key1)
                trie.add(charset, String.valueOf(aKey1), false);
            trie.add(charset, String.valueOf(" "), false);
        }
    }

    @Override
    public boolean equals(Object l) {
        return !(l == null || !(l instanceof Language)) && ((Language) l).fileName.equals(this.fileName);
    }
}
