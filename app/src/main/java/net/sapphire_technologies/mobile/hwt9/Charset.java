package net.sapphire_technologies.mobile.hwt9;

/**
 * Created by netman on 1/04/2017.
 *
 * Charset abstract
 */
public abstract class Charset {
    public abstract String getName();
    public abstract String getVisibleName();
    public abstract String getAbc();
    public abstract char[] getMap(char n);
    public abstract char getMapReverse(char c);
    public abstract char[] getSpecial();
    public abstract boolean isWordCharacter(char c);
    public abstract boolean needCapital(String bc, int textMode);

    @Override
    public boolean equals(Object cs) {
        return !(cs == null || !(cs instanceof Charset)) && ((Charset) cs).getName().equals(this.getName());
    }
}
