package net.sapphire_technologies.mobile.hwt9;

/**
 * Created by netman on 21/03/2017.
 *
 * Abstract for key handler
 */
abstract class KeyHandler {
    @SuppressWarnings("UnusedParameters")
    KeyHandler(HWT9 hwt9, HWT9.InputContext context) {
        /* Empty */
    }

    public abstract void finish();
    public abstract void cancel(); /* Cancel last character and finish, for long-press handler */
    public abstract boolean keyDown(int keyCode, boolean isCapitalized);
    public abstract void selChanged(int selStart, int selEnd, int cStart, int cEnd);
}
