package net.sapphire_technologies.mobile.hwt9;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by netman on 22/03/2017.
 *
 * Helper library to glue T9Predict onto the Trie
 */
class TrieQuery {
    public final static int maxlen = 512; /* Setting? */
    private final char[] currentWord = new char[maxlen];
    private final boolean[] currentWord_capitals = new boolean[maxlen];
    private int currentWord_pos = 0;
    private ArrayList<String> result;
    private int resultIndex;
    private int queryLength;
    private String word = "", prevWord = null; /* Keep word always to a string, prevword == null means no prevword */
    private boolean isModified = false; /* Shows whether modification happened after last reset or setword */
    private final HWT9 hwt9;
    private final HWT9.InputContext context;

    public TrieQuery(HWT9 hwt9, HWT9.InputContext context) {
        this.hwt9 = hwt9;
        this.context = context;
        setResult(null);
        setResultIndex(0);
    }

    public void onLanguageChanged() {
        updateFromWord(true);
    }

    public String getWord() {
        return word;
    }

    public boolean isEditing() {
        return word.length() > 0;
    }

    public boolean getIsModified() {
        return isModified;
    }

    public void setWord(String w, boolean[] capitals) {
        prevWord = null;
        word = "";
        isModified = false;
        if (capitals != null) {
            for (int i = currentWord_pos; i < capitals.length && i < currentWord_capitals.length; ++i)
                currentWord_capitals[i] = capitals[i];
        } else {
            for (int i = 0; i < w.length() && i < currentWord_capitals.length; ++i)
                currentWord_capitals[i] = Character.isUpperCase(w.charAt(i));
        }
        for (int i = 0; i < w.length(); ++i)
            word += (currentWord_capitals[i] ? Character.toUpperCase(w.charAt(i)) : w.charAt(i));
        updateFromWord(true);
    }

    public boolean key(char n, boolean isCapitalized) {
        prevWord = word;
        if (currentWord_pos == currentWord.length)
            --currentWord_pos; /* Just get stuck at the end, meh :-P */
        currentWord_capitals[currentWord_pos] = isCapitalized;
        currentWord[currentWord_pos++] = n;
        /* Check if that finds new results */
        ArrayList<String> tr = search();
        if (tr == null) {
            --currentWord_pos;
            return false;
        }
        setResult(tr);
        /* We added a key, should ditch the index */
        if (result.get(0).length() != currentWord_pos) {
            /* This happens if there's only partial matches and the truncated ones aren't shown */
            /* I.E. noPartial == true && expandedPartial == true */
            setWord(result.get(0).substring(0, currentWord_pos), new boolean[] {});
            setResultIndex(-1);
        } else setResultIndex(0);
        isModified = true;
        updateFromResult(false);
        return true;
    }

    public boolean backspace() {
        if (word.length() > 0) {
            prevWord = null;
            word = word.substring(0, word.length() - 1);
            isModified = true;
            /* Search if the word exists */
            updateFromWord(hwt9.prefs.keepRotation);
            return true;
        }
        return false;
    }

    public void next() {
        if (result == null || ++resultIndex >= result.size())
            setResultIndex(0);
        else setResultIndex(resultIndex); /* ++ only does not cut it */
        prevWord = word;
        isModified = true;
        updateFromResult(true);
    }

    public boolean select(int i) { /* Also returns whether it's different or not */
        if (i == resultIndex)
            return false; /* So our setResultIndex doesn't destroy prevWord */
        setResultIndex(i);
        updateFromResult(true);
        return true;
    }

    public void cancel() { /* Undo the last keydown, only for keydown things! */
        if (prevWord == null) {
            /* Backspace but always keep rotation */
            if (word.length() > 0) {
                word = word.substring(0, word.length() - 1);
                isModified = true;
                updateFromWord(true);
            }
            return;
        }
        word = prevWord;
        prevWord = null;
        isModified = true;
        updateFromWord(true);
    }

    public void reset() { /* Clear the query so we're ready for a new one */
        currentWord_pos = 0;
        setResult(null);
        setResultIndex(0);
        prevWord = word = "";
        isModified = false;
    }

    public int getResultCount() {
        return (result == null) ? 0 : result.size();
    }

    public void updateCapitals() {
        setResult(result);
    }

    private void updateFromWord(boolean find) {
        /* Note that this keeps the capitalization of currentWord_capitals */
        if (word.length() > maxlen)
            word = word.substring(0, maxlen);
        currentWord_pos = word.length();
        for (int i = 0; i < word.length(); ++i)
            currentWord[i] = hwt9.languages.getMapReverse(Character.toLowerCase(word.charAt(i)));
        setResult(search());
        if (find) {
            setResultIndex(-1); /* -1 index means word somehow not listed */
            if (result != null) {
                int len = result.size();
                for (int i = 0; i < len; ++i) {
                    if (result.get(i).toLowerCase().equals(word.toLowerCase())) {
                        setResultIndex(i);
                        break;
                    }
                }
            }
        } else {
            if (result != null && result.get(0).length() == currentWord_pos)
                setResultIndex(0);
            else setResultIndex(-1);
        }
        updateFromResult(false);
    }

    private void updateFromResult(boolean grow) {
        if (result == null || resultIndex == -1) /* -1 index means word somehow not listed */
            return;
        String res = result.get(resultIndex);
        if (grow) {
            int len = res.length();
            if (res.length() > maxlen) {
                res = res.substring(0, maxlen);
                len = res.length();
            }
            /* If the result is growing, we add the appropriate keypresses */
            if (queryLength < len)
                for (int i = queryLength; i < len; ++i)
                    currentWord[i] = hwt9.languages.getMapReverse(Character.toLowerCase(res.charAt(i)));
            currentWord_pos = len;
        } else res = res.substring(0, currentWord_pos);
        word = "";
        for (int i = 0; i < currentWord_pos; ++i)
            word += (currentWord_capitals[i] ? Character.toUpperCase(res.charAt(i)) : res.charAt(i));
    }

    private void setResult(ArrayList<String> r) {
        result = r;
        queryLength = currentWord_pos; /* Between search and setResult, no not modify queryLength */
        /* NOTE currentWord_pos is now only usable for this purpose updateFromResult(), after which it may be grown */
        if (result != null) {
            /* Initialize currentWord_capitals if necessary */
            int max = 0;
            for (String i : result)
                if (i.length() > max)
                    max = i.length();
            for (int i = currentWord_pos; i < max; ++i)
                currentWord_capitals[i] = (context.textMode.mode == TextMode.UPPERCASE);
            /* If we only have elongated partial matches, add the word */
            /* NOTE If unknown words in the result set is unwanted this can be safely removed */
            /*   But also remove the below else block */
            String first = result.get(0);
            if (first.length() > queryLength)
                result.add(0, first.substring(0, queryLength)); /* TODO somehow color this in PredictionsView and allow adding to dictionary */
        } else {
            result = new ArrayList<>();
            result.add(word); /* TODO somehow color this in PredictionsView and allow adding to dictionary */
        }
        /* Give our predicitons to the view (if any) */
        if (currentWord_pos == 0 || result == null)
            hwt9.predictionView.setPredictions(new ArrayList<String>(), currentWord_capitals);
        else hwt9.predictionView.setPredictions(result, currentWord_capitals);
    }

    private void setResultIndex(int i) {
        resultIndex = i;
        if (resultIndex >= 0)
            hwt9.predictionView.setPredictionIndex(resultIndex);
    }

    private ArrayList<String> search() {
        Set<String> res = new LinkedHashSet<>();
        ArrayList<Trie.Result> results = new ArrayList<>();
        /* Turn off partial matching if necessary */
        boolean origExpandedPartial = hwt9.prefs.expandedPartial;
        if (hwt9.prefs.limitExpanded && currentWord_pos > 0 && currentWord[currentWord_pos - 1] == 1)
            hwt9.prefs.expandedPartial = false;
        /* Search */
        results.add(hwt9.languages.userDictionary.trie.search(currentWord, currentWord_pos));
        for (Language l : hwt9.languages.addons)
            results.add(l.trie.search(currentWord, currentWord_pos));
        results.add(hwt9.languages.current.userDictionary.trie.search(currentWord, currentWord_pos));
        results.add(hwt9.languages.current.trie.search(currentWord, currentWord_pos));
        if (hwt9.prefs.combineLanguages)
            for (Language l : hwt9.languages)
                if (l != hwt9.languages.current) {
                    results.add(l.userDictionary.trie.search(currentWord, currentWord_pos));
                    results.add(l.trie.search(currentWord, currentWord_pos));
                }
        /* Put back extended partial matching to what it was */
        hwt9.prefs.expandedPartial = origExpandedPartial;
        /* Return the results in a sensible order */
        for (Trie.Result cr : results)
            res.addAll(cr.complete); /* According to docs, this shouldn't push existing elements down */
        for (Trie.Result cr : results)
            res.addAll(cr.partial);
        if (res.size() == 0)
            return null;
        return new ArrayList<>(res);
    }
}
