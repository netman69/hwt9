package net.sapphire_technologies.mobile.hwt9;

import android.inputmethodservice.InputMethodService;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;

import java.util.ArrayList;

/**
 * Created by netman on 11/04/2017.
 *
 * This is a wrapper for InputMethodService that adds some functionality and works around bugs
 * It serves to keep HWT9.java slightly cleaner
 */
public abstract class HWT9InputMethodService extends InputMethodService {
    private final ArrayMap<OnKeyUpEventHandler> events_keyUp = new ArrayMap<>();
    private final ArrayList<Runnable> events_longPress = new ArrayList<>();
    private final Handler handler = new Handler();

    long longPressTime = 1000;

    public interface OnKeyUpEventHandler {
        @SuppressWarnings("SameReturnValue")
        boolean onKeyUpEvent(@SuppressWarnings("UnusedParameters") int keyCode, @SuppressWarnings("UnusedParameters") KeyEvent event);
    }

    public interface OnLongPressEventHandler {
        void onLongPressEvent(int keyCode);
    }

    protected abstract boolean mOnKeyDown(int keyCode, KeyEvent event);
    protected abstract void mOnUpdateSelection(int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd);

    public void registerOnKeyUpEventHandler(final int keyCode, final OnKeyUpEventHandler evh) {
        events_keyUp.add(keyCode, evh);
    }

    void registerLongPressEventHanlder(final int keyCode, final OnLongPressEventHandler evh) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                evh.onLongPressEvent(keyCode);
            }
        };
        events_longPress.add(runnable);
        handler.postDelayed(runnable, longPressTime);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        /* Cancel running long-press handlers */
        if (event.getRepeatCount() == 0) {
            for (Runnable runnable : events_longPress)
                handler.removeCallbacks(runnable);
            events_longPress.clear();
        }
        /* Call handler for subclass */
        return mOnKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        ArrayList<OnKeyUpEventHandler> evhs_ku = events_keyUp.get(keyCode);
        events_keyUp.remove(keyCode);
        for (Runnable runnable : events_longPress)
            handler.removeCallbacks(runnable);
        events_longPress.clear();
        for (OnKeyUpEventHandler evh : evhs_ku)
            if (evh.onKeyUpEvent(keyCode, event))
                return true;
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onUpdateSelection(int oldSelStart, int oldSelEnd, int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) {
        /* Sometimes onUpdateSelection events come too late */
        InputConnection ic = getCurrentInputConnection();
        if (ic == null) /* And so this happens before */
            return;
        ExtractedTextRequest req = new ExtractedTextRequest();
        req.hintMaxChars = 0; /* We don't need any actual text */
        ExtractedText et = ic.getExtractedText(req, 0);
        /* So the solution is to simply ditch the event if it does not match reality */
        if (et != null && et.selectionEnd == newSelEnd && et.selectionStart == newSelStart)
            mOnUpdateSelection(newSelStart, newSelEnd, candidatesStart, candidatesEnd);
    }
}
