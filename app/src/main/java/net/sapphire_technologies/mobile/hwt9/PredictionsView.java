package net.sapphire_technologies.mobile.hwt9;

import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by netman on 28/03/2017.
 *
 * Handles the showing of candidates for word prediction
 */
@SuppressWarnings("deprecation")
public class PredictionsView {
    private final HWT9 hwt9;
    private ArrayList<String> predictionWords;
    private boolean[] predictionCapitals;
    private int predictionIndex = 0;

    private final ArrayList<Button> buttons = new ArrayList<>();

    private final View.OnClickListener predictionOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            hwt9.contextNormal.predict.selectPrediction(id);
            v.setFocusableInTouchMode(true);
            v.requestFocus();
            v.setFocusableInTouchMode(false);
        }
    };

    private final Runnable setPredictionsRunnable = new Runnable() {
        @Override
        public void run() {
            if (hwt9.kv == null)
                return;
            View sv = hwt9.kv.findViewById(R.id.predictionsScrollView);
            if (predictionWords == null || predictionWords.size() == 0 || !hwt9.prefs.showPredictions) {
                sv.setVisibility(View.INVISIBLE);
                //hwt9.kv.findViewById(R.id.language).setVisibility(View.VISIBLE);
                return;
            }
            sv.setVisibility(View.VISIBLE);
            //hwt9.kv.findViewById(R.id.language).setVisibility(View.GONE);
            LinearLayout ll = (LinearLayout) hwt9.kv.findViewById(R.id.predictions);
            int i;
            for (i = 0; i < predictionWords.size(); ++i) {
                if (buttons.size() <= i) {
                    Button b = new Button(hwt9);
                    buttons.add(b);
                    LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    par.setMargins(5, 10, 5, 10);
                    b.setLayoutParams(par);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        b.setAllCaps(false);
                    }
                    b.setMinHeight(0);
                    b.setPadding(10, 0, 10, 0);
                    b.setFocusableInTouchMode(false);
                    b.setBackgroundDrawable(hwt9.getResources().getDrawable(R.drawable.predictionbutton));
                    b.setTextColor(hwt9.getResources().getColor(R.color.button_text));
                    b.setId(i);
                    b.setOnClickListener(predictionOnClick);
                    ll.addView(b);
                }
                String word = "";
                String wordOrg = predictionWords.get(i);
                if (hwt9.prefs.capitalizePredictions) {
                    for (int c = 0; c < wordOrg.length() && c < predictionCapitals.length; ++c)
                        if (predictionCapitals[c])
                            word += Character.toUpperCase(wordOrg.charAt(c));
                        else word += wordOrg.charAt(c);
                } else word = wordOrg;
                Button b = buttons.get(i);
                b.setText(word);
                if (b.getParent() == null) /* Make sure the button is in the ViewGroup */
                    ll.addView(b);
                b.setVisibility(View.VISIBLE);
            }
            int nbuttons = buttons.size();
            for (; i < nbuttons; ++i) {
                Button b = buttons.get(i);
                if (b.getVisibility() == View.GONE)
                    break;
                b.setVisibility(View.GONE);
            }
            View v = hwt9.kv.findViewById(R.id.mainLayout);
            v.requestFocus();
        }
    };

    private final Runnable setPredictionIndexRunnable = new Runnable() {
        @Override
        public void run() {
            if (hwt9.kv == null)
                return;
            LinearLayout ll = (LinearLayout) hwt9.kv.findViewById(R.id.predictions);
            Button b = (Button) ll.findViewById(predictionIndex);
            if (b != null) {
                b.setFocusableInTouchMode(true);
                b.requestFocus();
                b.setFocusableInTouchMode(false);
            }
        }
    };

    public PredictionsView(HWT9 hwt9) {
        this.hwt9 = hwt9;
    }

    public void setPredictions(ArrayList<String> words, boolean[] capitals) {
        predictionWords = words;
        predictionCapitals = capitals;
        /* We use a handler because it's apparently faster */
        hwt9.handler.removeCallbacks(setPredictionsRunnable);
        hwt9.handler.post(setPredictionsRunnable);
    }

    public void setPredictionIndex(int i) {
        predictionIndex = i;
        /* We simply use a handler so it can behave synchronously with setPredictions */
        hwt9.handler.removeCallbacks(setPredictionIndexRunnable);
        hwt9.handler.post(setPredictionIndexRunnable);
    }

    public void clearParent() {
        ((LinearLayout) hwt9.kv.findViewById(R.id.predictions)).removeAllViews();
    }

    public void updateView() {
        setPredictions(predictionWords, predictionCapitals);
        setPredictionIndex(predictionIndex);
    }
}
