package net.sapphire_technologies.mobile.hwt9;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;

/**
 * Created by netman on 25/03/2017.
 *
 * This is for glueing the XML preferences onto the rest of the app
 */
@SuppressWarnings("deprecation")
public class PreferencesDialog extends android.preference.PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceScreen screen = getPreferenceScreen();
        screen.getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        screen.findPreference("pref_key_languages").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Languages.scanStatic(getApplicationContext());
                Intent i = new Intent(getApplicationContext(), LanguagesDialog.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                return true;
            }
        });
        screen.findPreference("pref_key_languagesReload").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent i = new Intent();
                i.setAction("net.sapphire_technologies.mobile.hwt9.languagesReload");
                sendBroadcast(i);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Intent i = new Intent();
        i.setAction("net.sapphire_technologies.mobile.hwt9.settingsChanged");
        sendBroadcast(i);
    }
}
