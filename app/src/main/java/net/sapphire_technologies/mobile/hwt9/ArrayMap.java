package net.sapphire_technologies.mobile.hwt9;

import java.util.ArrayList;

/**
 * Created by netman on 11/04/2017.
 *
 * An Array where things have a (not necessarly unique) identifying number that can be used to search.
 */
class ArrayMap<T> {
    private class Tuple {
        final int idx;
        final T data;

        Tuple(int idx, T data) {
            this.idx = idx;
            this.data = data;
        }
    }

    private final ArrayList<Tuple> items = new ArrayList<>();

    /* Add item with specified identifier */
    public void add(int id, T element) {
        items.add(new Tuple(id, element));
    }

    /* Get all items with a specified identifier */
    public ArrayList<T> get(int id) {
        ArrayList<T> ret = new ArrayList<>();
        for (Tuple t : items)
            if (t.idx == id)
                ret.add(t.data);
        return ret;
    }

    /* Remove all items with a specified identifier */
    public void remove(int id) {
        ArrayList<Tuple> toRemove = new ArrayList<>();
        for (Tuple t : items)
            if (t.idx == id)
                toRemove.add(t);
        for (Tuple t : toRemove)
            items.remove(t);
    }

// --Commented out by Inspection START (11/04/2017 23:29):
//    /* Remove a specific object */
//    public void remove(T element) {
//        ArrayList<Tuple> toRemove = new ArrayList<>();
//        for (Tuple t : items)
//            if (t.data.equals(element))
//                toRemove.add(t);
//        for (Tuple t : toRemove)
//            items.remove(t);
//    }
// --Commented out by Inspection STOP (11/04/2017 23:29)
}
