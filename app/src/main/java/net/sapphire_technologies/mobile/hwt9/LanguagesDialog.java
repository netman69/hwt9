package net.sapphire_technologies.mobile.hwt9;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;

import java.util.Set;

/**
 * Created by netman on 2/04/2017.
 *
 * Dialog to allow the user to select what languages to enable
 */
@SuppressWarnings("deprecation")
public class LanguagesDialog extends android.preference.PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager pm = getPreferenceManager();
        pm.setSharedPreferencesName("hwt9_languages");
        PreferenceScreen screen = pm.createPreferenceScreen(this);
        final SharedPreferences sp = screen.getSharedPreferences();

        final PreferenceCategory charsetCategory = new PreferenceCategory(this);
        charsetCategory.setTitle(R.string.pref_category_charsets);
        screen.addPreference(charsetCategory);

        /* Get list of existing charsets */
        Charset[] sets = Charsets.getAll();
        for (Charset set : sets) {
            String key = 'c' + set.getName();
            String name = set.getVisibleName();
            CheckBoxPreference checkBoxPref = new CheckBoxPreference(this);
            checkBoxPref.setTitle(name);
            checkBoxPref.setSummary(getString(R.string.pref_summary_charset));
            checkBoxPref.setKey(key);
            checkBoxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    ((CheckBoxPreference) preference).setChecked((Boolean) newValue);
                    preventUncheckLast(sp, charsetCategory);
                    return false;
                }
            });
            charsetCategory.addPreference(checkBoxPref);
        }
        preventUncheckLast(sp, charsetCategory);

        PreferenceCategory category = new PreferenceCategory(this);
        category.setTitle(R.string.pref_category_languages);
        screen.addPreference(category);

        /* Get a list of all the languages in our SharedPreferences */
        Set<String> keys = sp.getAll().keySet();
        for (String key : keys) {
            if (key.charAt(0) != 'f')
                continue;
            String name = sp.getString('n' + key.substring(1), "ERROR");
            CheckBoxPreference checkBoxPref = new CheckBoxPreference(this);
            checkBoxPref.setTitle(name);
            checkBoxPref.setSummary(String.format(getString(R.string.pref_summary_language), key.substring(1)));
            checkBoxPref.setKey(key);
            category.addPreference(checkBoxPref);
        }

        category = new PreferenceCategory(this);
        category.setTitle(R.string.pref_category_addondictionaries);
        screen.addPreference(category);

        /* Get a list of all the languages in our SharedPreferences */
        keys = sp.getAll().keySet();
        for (String key : keys) {
            if (key.charAt(0) != 'a')
                continue;
            String name = sp.getString('n' + key.substring(1), "ERROR");
            CheckBoxPreference checkBoxPref = new CheckBoxPreference(this);
            checkBoxPref.setTitle(name);
            checkBoxPref.setSummary(String.format(getString(R.string.pref_summary_addondictionary), key.substring(1)));
            checkBoxPref.setKey(key);
            category.addPreference(checkBoxPref);
        }

        setPreferenceScreen(screen);
        sp.registerOnSharedPreferenceChangeListener(this);
    }

    private void preventUncheckLast(SharedPreferences sp, PreferenceCategory category) {
        int checked = 0;
        CheckBoxPreference lastEnabled = null;
        for (int i = 0, n = category.getPreferenceCount(); i < n; ++i) {
            CheckBoxPreference pref = (CheckBoxPreference) category.getPreference(i);
            if (sp.getBoolean(pref.getKey(), false)) {
                ++checked;
                lastEnabled = pref;
            }
            pref.setEnabled(true);
        }
        if (checked == 1) {
            lastEnabled.setEnabled(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Intent i = new Intent();
        i.setAction("net.sapphire_technologies.mobile.hwt9.languagesChanged");
        sendBroadcast(i);
    }
}
