package net.sapphire_technologies.mobile.hwt9;

import android.view.KeyEvent;

/**
 * Created by netman on 10/04/2017.
 *
 * Some abstraction for key input
 */
class Keys {
    private static final int KEY_UNKNOWN = 0;
    public static final int KEY_OK = 1;
    public static final int KEY_BACK = 2;

    public static int getAbstractedKey(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_NUMPAD_ENTER:
                return KEY_OK;
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
                return KEY_BACK;
        }
        return KEY_UNKNOWN;
    }
}
