package net.sapphire_technologies.mobile.hwt9;

import android.view.KeyEvent;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;

/**
 * Created by netman on 3/18/17.
 *
 * Predictive input KeyHandler
 */
public class T9Predict extends KeyHandler {
    private final TrieQuery tq;
    private boolean isSpace = false, stopComposing = false;
    private final HWT9 hwt9;
    private final HWT9.InputContext context;

    public T9Predict(HWT9 hwt9, HWT9.InputContext context) {
        super(hwt9, context);
        this.hwt9 = hwt9;
        this.context = context;
        tq = new TrieQuery(hwt9, context);
    }

    public void onLanguageChanged() {
        tq.onLanguageChanged();
    }

    public void onTextModeChanged() {
        tq.updateCapitals();
    }

    @Override
    public void finish() {
        if (context.ic != null)
            context.ic.finishComposingText();
        tq.reset();
    }

    @Override
    public void cancel() {
        tq.cancel();
        context.ic.setComposingText(tq.getWord(), 1);
    }

    @Override
    public boolean keyDown(int keyCode, boolean isCapitalized) {
        /* Keep in mind that key 1 (punctuation) is special. */
        char n = Tables.getKCNum(keyCode);
        endSpace(); /* If space key is held down, end "editing" the space */

        /* Handle dpad keys */
        if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT)
            return goRight();
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT)
            return goLeft();

        /* NOTE Do not forget to endBatchEdit! */
        context.ic.beginBatchEdit();

        /* Handle * key */
        if (keyCode == KeyEvent.KEYCODE_STAR) {
            /* Follow long-press events on this, to show star menu */
            hwt9.registerLongPressEventHanlder(keyCode, new HWT9InputMethodService.OnLongPressEventHandler() {
                @Override
                public void onLongPressEvent(int keyCode) {
                    if (keyCode == KeyEvent.KEYCODE_STAR) {
                        new Star(hwt9, context);
                        if (!hwt9.languages.cancel()) /* Won't affect normal mode cause then we ain't grabbing long-press */
                            cancel();
                    }
                }
            });
            /* Do the normal action already, the long press thing will cancel */
            if (!tq.isEditing()) {
                context.ic.endBatchEdit();
                return false;
            }
            String word = tq.getWord().toLowerCase();
            /* If we have a special character on the end of a word (not alone) and no completions, separate it */
            if (tq.getResultCount() <= 1 && word.length() >= 2 &&
                    hwt9.languages.getMapReverse(word.charAt(word.length() - 1)) == 1) {
                tq.cancel();
                context.ic.setComposingText(tq.getWord(), 1);
                finish();
                //noinspection ConstantConditions
                tq.setWord(String.valueOf(hwt9.languages.current.charset.getMap((char) 1)[0]), null);
                context.ic.setComposingText(tq.getWord(), 1);
                context.ic.endBatchEdit();
                return true;
            }
            /* If we have a special character and then a letter, allow breaking it off similarly */
            if (tq.getResultCount() <= 1 && word.length() >= 2 &&
                    hwt9.languages.getMapReverse(word.charAt(word.length() - 2)) == 1) {
                char c = hwt9.languages.getMapReverse(word.charAt(word.length() - 1));
                if (c <= 9) {
                    tq.cancel();
                    context.ic.setComposingText(tq.getWord(), 1);
                    finish();
                    //noinspection ConstantConditions
                    tq.setWord(String.valueOf(hwt9.languages.current.charset.getMap(c)[0]), null);
                    context.ic.setComposingText(tq.getWord(), 1);
                    context.ic.endBatchEdit();
                    return true;
                }
            }
            /* Rotate around the completions */
            tq.next();
            context.ic.setComposingText(tq.getWord(), 1);
            context.ic.endBatchEdit();
            return true;
        }

        if (n == 0) {
            /* Handle space key, it should always just put a space */
            finish();
            context.ic.setComposingText(" ", 1);
            context.ic.endBatchEdit();
            isSpace = true;
            hwt9.registerOnKeyUpEventHandler(keyCode, new HWT9InputMethodService.OnKeyUpEventHandler() {
                @Override
                public boolean onKeyUpEvent(int keyCode, KeyEvent event) {
                    endSpace();
                    return true;
                }
            });
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DEL) { /* It means backspace */
            if (tq.backspace()) {
                context.ic.setComposingText(tq.getWord(), 1);
                context.ic.endBatchEdit();
                return true;
            } else {
                context.ic.endBatchEdit();
                return false;
            }
        } else if (n < 10) {
            if (!tq.key(n, isCapitalized)) {
                /* If key 1 and no new results */
                if (n == 1) {
                    /* We finish the composing, and start a new word with the special char */
                    finish();
                    //noinspection ConstantConditions
                    tq.setWord(String.valueOf(hwt9.languages.current.charset.getMap((char) 1)[0]), null);
                    context.ic.setComposingText(tq.getWord(), 1);
                    context.ic.endBatchEdit();
                    return true;
                }
                /* If last character is special, start a new word */
                String w = tq.getWord();
                if (w.length() > 0 && hwt9.languages.getMapReverse(w.charAt(w.length() - 1)) == 1) {
                    finish();
                    tq.key(n, isCapitalized);
                    context.ic.setComposingText(tq.getWord(), 1);
                    context.ic.endBatchEdit();
                    return true;
                }
                hwt9.status.addTimed(hwt9.getString(R.string.status_noResults), false);
            }
            context.ic.setComposingText(tq.getWord(), 1);
            context.ic.endBatchEdit();
            return true;
        }
        context.ic.endBatchEdit();
        return false;
    }

    @Override
    public void selChanged(int selStart, int selEnd, int cStart, int cEnd) {
        if (stopComposing) {
            stopComposing = false;
            return;
        }
        if (selStart != selEnd) {
            /* This allows new input to overwrite selected text */
            finish();
        } else if ((selEnd <= cStart || selEnd > cEnd) || cStart == cEnd) {
            /* Here means we moved out of the composing region */
            /* We look if there's a word here */
            int cursor, pos, start, end;
            String text;
            CharSequence before = context.ic.getTextBeforeCursor(TrieQuery.maxlen, 0);
            CharSequence after = context.ic.getTextAfterCursor(TrieQuery.maxlen, 0);
            if (before == null || after == null)
                return;
            cursor = before.length();
            text = before.toString() + after.toString();
            /* Search in reverse until non-word character is found */
            pos = cursor;
            while (pos > 0 && hwt9.languages.isWordCharacter(text.charAt(pos - 1)))
                pos--;
            start = pos;
            /* Search forward until non-word character is found */
            pos = cursor;
            if (start != cursor) { /* If we're all the way at the left it don't count */
                while (pos < text.length() && hwt9.languages.isWordCharacter(text.charAt(pos)))
                    ++pos;
            }
            end = pos;
            /* If nothing found check for a special char */
            if (start == end && before.length() > 0) {
                char c = before.charAt(before.length() - 1);
                if (c != '1' && hwt9.languages.getMapReverse(c) == 1) {
                    start = cursor - 1;
                    end = cursor;
                }
            }
            /* Set the composing region and find the word */
            int newStart = selEnd - (cursor - start), newEnd = selEnd + (end - cursor);
            if (newStart != cStart || newEnd != cEnd) /* Don't update needlessly */
                context.ic.setComposingRegion(newStart, newEnd);
            /* Find the word if it exists */
            /* NOTE setWord might truncate the string if it is larger than maxlen */
            tq.reset();
            tq.setWord(text.substring(start, end), null);
        }
    }

    public void setWord(String w, boolean[] capitals) {
        tq.setWord(w, capitals);
        context.ic.setComposingText(tq.getWord(), 1);
    }

    public String getWord() {
        if (isSpace)
            return "";
        return tq.getWord();
    }

    private boolean goRight() {
        CharSequence right = context.ic.getTextAfterCursor(TrieQuery.maxlen, 0);
        int d;
        if (right == null)
            return false;
        //noinspection StatementWithEmptyBody
        for (d = 0; d < right.length() && hwt9.languages.isWordCharacter(right.charAt(d)); ++d);
        if (d == 0) {
            int wl = tq.getWord().length();
            if ((hwt9.prefs.fastArrows && (tq.getIsModified() || (right.length() == 0 && wl > 0))) ||
               (!hwt9.prefs.fastArrows && (wl > 0))) {
                stopComposing = true; /* This stops selChanged from getting back our composing region */
                finish();
                return true;
            }
            return false;
        }
        context.ic.beginBatchEdit();
        finish();
        moveCursor(d);
        context.ic.endBatchEdit();
        return true;
    }

    private boolean goLeft() {
        CharSequence left = context.ic.getTextBeforeCursor(TrieQuery.maxlen, 0);
        int d;
        if (left == null || left.length() <= 1)
            return false;
        //noinspection StatementWithEmptyBody
        for (d = left.length(); d > 0 && hwt9.languages.isWordCharacter(left.charAt(d - 1)); --d);
        d = left.length() - d;
        if (d == 0)
            return false;
        context.ic.beginBatchEdit();
        finish();
        moveCursor(-d);
        context.ic.endBatchEdit();
        return true;
    }

    private void moveCursor(int offset) {
        ExtractedTextRequest req = new ExtractedTextRequest();
        req.hintMaxChars = 0; /* We don't need any actual text */
        if (context.ic == null)
            return; /* It happens before */
        ExtractedText et = context.ic.getExtractedText(req, 0);
        int pos = et.selectionStart + offset;
        context.ic.setSelection(pos, pos);
    }

    public void selectPrediction(int i) {
        if (tq.select(i))
            context.ic.setComposingText(tq.getWord(), 1);
    }

    private void endSpace() {
        if (isSpace) {
            context.ic.finishComposingText();
            isSpace = false;
        }
    }
}
