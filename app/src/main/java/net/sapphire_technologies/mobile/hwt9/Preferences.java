package net.sapphire_technologies.mobile.hwt9;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by netman on 25/03/2017.
 *
 * Preferences that come as normal variable from SharedPreferences
 */
class Preferences {
    private final HWT9 hwt9;
    private final SharedPreferences sPrefs;

    /* Receiver to update settings */
    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            update();
            hwt9.onSettingsChanged();
        }
    };

    /* General */
    public long longPressTime = 750; /* How long counts as long-press (ms) */
    public long textModeSwitchTime = 500; /* How long is double-press for mode switch */
    public boolean starDismissOnChar = true; /* Dismiss special char dialog */
    public long tempStatusTime = 1000; /* How long to show temporary status messages */

    /* Predictive mode */
    public boolean showPredictions = true;
    public boolean capitalizePredictions = true;
    public boolean fastArrows = true; /* If disabled, DPAD_RIGHT stops composing before moving to the right */
    public boolean keepRotation = false; /* If true, backspace acts in found partial word */
    /* NOTE That full matches will still override partial backspaced ones if trie.noPartial = true */
    public boolean noPartial = false; /* Do not show partial matches if complete ones found */
    public boolean expandedPartial = true;
    public boolean limitExpanded = true;
    public boolean combineLanguages = false;
    public boolean addWordCurrent = true;
    @SuppressWarnings("CanBeFinal")
    public int maxPartial = 10; /* Maximum # of attempts to find partial matches */

    /* Normal mode */
    public int commitDelay = 500; /* Max delay between multiple key-presses to count as doubles */

    public Preferences(final HWT9 hwt9) {
        this.hwt9 = hwt9;
        sPrefs = PreferenceManager.getDefaultSharedPreferences(hwt9);
        PreferenceManager.setDefaultValues(hwt9, R.xml.preferences, false);
        update();
    }

    private void update() {
        /* General */
        longPressTime = Integer.valueOf(sPrefs.getString("pref_key_longPressTime", "750")); /* Somehow the ListPreference only works for strings */
        textModeSwitchTime = Integer.valueOf(sPrefs.getString("pref_key_textModeSwitchTime", "500"));
        starDismissOnChar = sPrefs.getBoolean("pref_key_starDismissOnChar", true);
        tempStatusTime = Integer.valueOf(sPrefs.getString("pref_key_tempStatusTime", "1000"));

        /* Predictive mode */
        showPredictions = sPrefs.getBoolean("pref_key_showPredictions", true);
        capitalizePredictions = sPrefs.getBoolean("pref_key_capitalizePredictions", true);
        fastArrows = sPrefs.getBoolean("pref_key_fastArrows", true);
        keepRotation = sPrefs.getBoolean("pref_key_keepRotation", false);
        noPartial = sPrefs.getBoolean("pref_key_noPartial", false);
        expandedPartial = sPrefs.getBoolean("pref_key_expandedPartial", true);
        limitExpanded = sPrefs.getBoolean("pref_key_limitExpanded", true);
        combineLanguages = sPrefs.getBoolean("pref_key_combineLanguages", false);
        addWordCurrent = sPrefs.getBoolean("pref_key_addWordCurrent", true);
        maxPartial = Integer.valueOf(sPrefs.getString("pref_key_maxPartial", "10"));

        /* Normal mode */
        commitDelay = Integer.valueOf(sPrefs.getString("pref_key_commitDelay", "500")); /* Somehow the ListPreference only works for strings */
    }
}
