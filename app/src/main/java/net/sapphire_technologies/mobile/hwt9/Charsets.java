package net.sapphire_technologies.mobile.hwt9;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 * Created by netman on 4/04/2017.
 *
 * Charset listing etc
 */
public class Charsets extends ArrayList<Charset> {
    private final HWT9 hwt9;

    public Charsets(HWT9 hwt9) {
        this.hwt9 = hwt9;
        scan();
    }

    public void scan() {
        SharedPreferences sp = hwt9.getSharedPreferences("hwt9_languages", Context.MODE_PRIVATE);
        Map<String, ?> map = sp.getAll();
        Set<String> keys = map.keySet();
        clear();
        for (String k : keys) {
            if (k.charAt(0) == 'c' && sp.getBoolean(k, false))
                add(get(k.substring(1)));
        }
        if (size() == 0) {
            Charset first = getAll()[0];
            add(first);
            SharedPreferences.Editor ed = sp.edit();
            ed.putBoolean('c' + first.getName(), true);
            ed.commit();
        }
        /* TODO cleanup */
        if (hwt9.contextNormal.textMode != null && !hwt9.contextNormal.textMode.isPredictive) {
            boolean ok = false;
            for (Charset cs : this)
                if (cs.equals(hwt9.contextNormal.textMode.charset))
                    ok = true;
            if (!ok)
                hwt9.contextNormal.textMode.setCharset(get(0));
        }
    }

    public static Charset[] getAll() {
        return new Charset[] { new CharsetLatin(), new CharsetRussian()  };
    }

    public static Charset get(String name) {
        Charset[] sets = getAll();
        for (Charset set : sets) {
            if (set.getName().equals(name))
                return set;
        }
        return null;
    }
}
