package net.sapphire_technologies.mobile.hwt9;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by netman on 3/19/17.
 *
 * The trie we built our house in
 */
public class Trie {
    private static class Node {
        public static class Sub {
            private char id = 255;
            private Node node;
            private Sub next;
        }

        public static class Word {
            final String word;
            Word next;

            public Word(String w) {
                word = w;
            }
        }

        public Sub sub;
        public Word firstWord;

        public Node getSub(char id) {
            Sub c = sub;
            while (c != null) {
                if (c.id == id)
                    return c.node;
                c = c.next;
            }
            return null;
        }

        public Node createSub(char id) {
            Sub c;
            Node n = getSub(id);
            if (n != null) /* If already exists, give that one */
                return n;
            if (sub == null)
                sub = new Sub();
            c = sub;
            while (c.next != null) /* Find the last sub */
                c = c.next; /* Keeping them sorted allows more sensible partial matches */
            if (c.id != 255) { /* 255 means it's a new one */
                c.next = new Sub(); /* If it's not new, append new one */
                c = c.next;
            }
            c.id = id;
            c.node = new Node();
            return c.node;
        }

        public void addWord(String w, boolean front) {
            Word nw = new Word(w);
            if (firstWord == null) {
                firstWord = nw;
            } else {
                if (front) {
                    /* Remove the word if it exists */
                    Word p = firstWord;
                    while (p.next != null) {
                        if (p.next.word.equals(w)) {
                            p.next = p.next.next;
                            break;
                        }
                        p = p.next;
                    }
                    /* Put our word in there at the top */
                    nw.next = firstWord;
                    firstWord = nw;
                } else {
                    Word p = firstWord;
                    while (p.next != null)
                        p = p.next;
                    p.next = new Word(w);
                }
            }
        }
    }

    private final Node root = new Node();
    @SuppressWarnings("FieldCanBeLocal")
    private final static int nsubnodes = 11;
    private final Object lock = new Object();
    private final Preferences prefs;

    public Trie(Preferences prefs) {
        this.prefs = prefs;
    }

    public void add(Charset charset, String word, boolean front) {
        Node p = root;
        String wordlc = word.toLowerCase(); /* We keep the capitalization in stored word */
        synchronized (lock) {
            for (int i = 0; i < word.length(); i++) {
                char index = charset.getMapReverse(wordlc.charAt(i));
                p = p.createSub(index);
            }
            p.addWord(word, front);
        }
    }

    class Result {
        final ArrayList<String> complete = new ArrayList<>();
        final ArrayList<String> partial = new ArrayList<>();
    }

    public Result search(char[] query, int len) {
        Node p = root;
        int i;
        char index;
        Set<String> dupcheck = new HashSet<>();
        Result res = new Result();

        if (len == 0)
            return new Result();

        synchronized (lock) {
            for(i = 0; i < len; i++){
                Node sn;
                index = query[i];
                if (p.sub != null && (sn = p.getSub(index)) != null) {
                    p = sn;
                } else break;
            }
            if (i < len)
                return new Result();
            /* Did we match any words? */
            if (p.firstWord != null) {
                for (Node.Word w = p.firstWord; w != null; w = w.next) {
                    if (!dupcheck.contains(w.word))
                        res.complete.add(w.word);
                    dupcheck.add(w.word);
                }
            }
            /* Search for partial matches */
            if ((!prefs.noPartial || prefs.expandedPartial) || p.firstWord == null) {
                for (int skip = 0; skip < prefs.maxPartial; ++skip) {
                    Node q = p;
                    int toskip = skip;
                    while (q != null && q.sub != null && (q.firstWord == null || q == p)) {
                        Node sn = null;
                        char c;
                        for (c = 0; c < nsubnodes; ++c)
                            if ((sn = q.getSub(c)) != null) {
                                if (toskip == 0)
                                    break;
                                else --toskip;
                            }
                        if (c == nsubnodes)
                            q = null;
                        else q = sn;
                    }
                    if (q == null)
                        break;
                    if (q.firstWord != null) {
                        for (Node.Word w = q.firstWord; w != null; w = w.next) {
                            if (!prefs.noPartial || !prefs.expandedPartial) {
                                String pw = w.word.substring(0, len);
                                if (!dupcheck.contains(pw))
                                    res.partial.add(pw);
                                dupcheck.add(pw);
                            }
                            if (prefs.expandedPartial) {
                                if (!dupcheck.contains(w.word))
                                    res.partial.add(w.word);
                                dupcheck.add(w.word);
                            }
                        }
                    }
                }
            }
        }

        return res;
    }
}
