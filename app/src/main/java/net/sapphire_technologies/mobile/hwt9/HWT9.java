package net.sapphire_technologies.mobile.hwt9;

import android.annotation.SuppressLint;
import android.content.IntentFilter;
import android.os.Handler;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by netman on 3/18/17.
 *
 * Hardware T9 IME
 */
public class HWT9 extends HWT9InputMethodService {
    /* TODO
     *
     * Critical:
     *  - Allow user to decide charset and language order
     *  - Enable a dictionary by default (or not?)
     *  - Make sure do not run out of memory while loading dictionaries (see also user dictionaries)
     *  - Custom dictionary editing (and remove words from predictions view)
     *  - Tool to clean up duplicates in user dictionary
     *  - Choose keybinds, choose visible buttons
     *
     *  - Figure out what to do with spanish upside down question mark (Extra charset? Charset addon things? How to do with multiple latin variations in normal mode?)
     *  - Apparently spanish also uses ñ as dedicated letter for 6 key mnño
     *  - Charset for having french ç more accessible
     *  - Add more contractions to english dictionary
     *  - Add contractions to french
     *  - About dialog to show licences for dictionaries (put link in settings)
     *  - Settings item that directs to help
     *  - Write help page:
     *      explanation of modes and how to switch (see code of mode switching comment)
     *      explain long press shows number
     *      how to punctuation
     *      switching languages
     *      adding words
     *      explain that if word changes by adding ' or such, * will cancel and go to punctuation
     *           the same for nose-less smilie like :D the letter part can be broken off with *
     *      tell how dictionary files should be (order by popularity, newline separated, capitals and specials allowed)
     *      explain builtin dictionary can be overridden by file with same filename
     *
     * Less important:
     *  - Better search for existing words when cursor moved (find back things such as smilies by scanning over groups of word-nonword things)
     *  - Software keypad for those who are left out :D
     *  - Customizable charsets
     *  - Do something with overriding void onDisplayCompletions(CompletionInfo[] completions) ?
     *  - Dark and light theme
     *  - Give the addword edit a theme
     *  - Give the loading animation some theme
     *  - Option to disable selecting charsets that do not belong to language in addword
     *  - Completions or corrections (corrections can use same trie) in normal mode (optional)
     *  - Add words that are typed in the editor manually or composed of multiple predicted words
     *  - Make trie search separate suffixes book
     *  - Option that makes double space put a .
     *  - Configurable key bindings
     *  - Action key when IME_FLAG_NO_ENTER_ACTION set (phone button?)
     *  - Display enter-action type
     *  - Way to select with dpad (arrows while holding down *?)
     *  - Reset settings option in settings
     *
     * Notes:
     *  - In messenger windows from the "chat heads" text selection doesn't work. This is for all keyboards.
     *
     */
    public final Handler handler = new Handler();
    public View kv; /* The main UI view */
    public PredictionsView predictionView;
    public Status status;
    public Languages languages;
    public Charsets charsets;
    public Preferences prefs;
    public final InputContext contextNormal = new InputContext();
    private InputContext contextCurrent = contextNormal;

    static class InputContext {
        public InputConnection ic;
        public EditorInfo editorInfo;
        public T9Normal normal;
        public T9Predict predict;
        public TextMode textMode;

        /* Give right keyHandler for current situation */
        private KeyHandler getKeyHandler() {
            if (textMode.mode == TextMode.NUMBERS)
                return null;
            if (textMode.isPredictive)
                return predict;
            else return normal;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        prefs = new Preferences(this);
        status = new Status(this);
        predictionView = new PredictionsView(this);
        charsets = new Charsets(this); /* Need this before TextMode */
        contextNormal.textMode = new TextMode(this, contextNormal);
        contextNormal.normal = new T9Normal(this, contextNormal);
        contextNormal.predict = new T9Predict(this, contextNormal);
        languages = new Languages(this);
        languages.scan();

        /* Register our receivers */
        registerReceiver(prefs.receiver, new IntentFilter("net.sapphire_technologies.mobile.hwt9.settingsChanged"));
        registerReceiver(languages.receiver, new IntentFilter("net.sapphire_technologies.mobile.hwt9.languagesChanged"));
        registerReceiver(languages.receiverReload, new IntentFilter("net.sapphire_technologies.mobile.hwt9.languagesReload"));

        onSettingsChanged(); /* Calls updateView() */
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /* On destroying we need to unregister the receivers */
        unregisterReceiver(prefs.receiver);
        unregisterReceiver(languages.receiver);
        unregisterReceiver(languages.receiverReload);
        /* Stop composing etc */
        endInput(contextNormal);
        /* We stop the loading threads */
        languages.stopLoading();
    }

    public void onSettingsChanged() {
        longPressTime = prefs.longPressTime;
        updateView();
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateInputView() {
        if (kv != null)
            predictionView.clearParent();
        kv = getLayoutInflater().inflate(R.layout.keyboard, null);
        Button b = (Button) kv.findViewById(R.id.addWord);
        b.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { addWord_start(); }
        });
        b = (Button) kv.findViewById(R.id.addWordButton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) { addWord_end(true); }
        });
        /* Make sure the text mode etc is shown correctly */
        updateView();
        /* Initialize the addword view */
        AddWordEdit addWordInput = (AddWordEdit) kv.findViewById(R.id.addWordEditor);
        addWordInput.setOnSelectionChangedListener(new AddWordEdit.OnSelectionChangedListener() {
            @Override
            public void onSelectionChanged(int selStart, int selEnd) {
                contextCurrent.textMode.updateView();
            }
        });
        return kv;
    }

    @Override
    public void onStartInput(EditorInfo attribute, boolean restarting) {
        /* Make sure we're ready for a new input field */
        if (addingWord)
            addWord_end(false);
        contextNormal.ic = getCurrentInputConnection(); /* We only receive this event for normal context */
        contextNormal.editorInfo = attribute;
        endInput(contextNormal); /* Redundant, I think */
        updateView();
        int inputClass = contextNormal.editorInfo.inputType & InputType.TYPE_MASK_CLASS;
        switch (inputClass) {
            case InputType.TYPE_CLASS_NUMBER:
                if (contextNormal.textMode.mode != TextMode.NUMBERS)
                    contextNormal.textMode.setTextMode(TextMode.NUMBERS, false, false, charsets.get(0));
                break;
            case InputType.TYPE_CLASS_TEXT:
            case InputType.TYPE_CLASS_DATETIME:
                if (contextNormal.textMode.mode == TextMode.NUMBERS)
                    contextNormal.textMode.setTextMode(TextMode.AUTO, true, false, charsets.get(0));
                break;
        }
    }

    @Override
    public void onFinishInput() {
        /* Make sure to be done with old input field */
        if (addingWord)
            addWord_end(false);
        endInput(contextNormal); /* We only receive this event for normal context */
    }

    @Override
    public boolean mOnKeyDown(int keyCode, KeyEvent event) {
        /* This function handles hardware keys coming in */
        /* Returning true eats the keycode, false lets other folks handle it */
        final InputContext context = contextCurrent;
        final KeyHandler kh = context.getKeyHandler();
        boolean isRepeat = event.getRepeatCount() > 0;
        int n = Tables.getKCNum(keyCode);
        InputConnection ic = context.ic;

        /* See if we should even continue */
        int inputClass = context.editorInfo != null ? context.editorInfo.inputType & InputType.TYPE_MASK_CLASS : EditorInfo.TYPE_NULL;
        if (!addingWord && (ic == null || inputClass == EditorInfo.TYPE_NULL || inputClass == EditorInfo.TYPE_CLASS_PHONE))
            return false;

        /* Get rid of repeated events */
        if (isRepeat && (keyCode == KeyEvent.KEYCODE_STAR || keyCode == KeyEvent.KEYCODE_POUND || (n >= 0 && n <= 9)))
            return true; /* Lose repeated events for most keys */

        /* Handle some keys in add word mode */
        if (addingWord) {
            if (keyCode == KeyEvent.KEYCODE_0) {
                addWordMode_rotate();
                registerLongPressEventHanlder(keyCode, new OnLongPressEventHandler() {
                    @Override
                    public void onLongPressEvent(int keyCode) {
                        addWordMode_rotate_cancel();
                        context.ic.commitText(Integer.toString(Tables.getKCNum(keyCode)), 1);
                    }
                });
                return true;
            }
            switch (Keys.getAbstractedKey(keyCode)) {
                case Keys.KEY_OK:
                    addWord_end(true);
                    return true;
                case Keys.KEY_BACK:
                    addWord_end(false);
                    return true;
                default:
                    ic.sendKeyEvent(event);
            }
        }

        /* Handle DPAD_CENTER thing */
        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            if ((context.editorInfo.imeOptions & EditorInfo.IME_FLAG_NO_ENTER_ACTION) == 0)
                ic.performEditorAction(context.editorInfo.imeOptions & EditorInfo.IME_MASK_ACTION);
            else ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
        }

        /* Handle # key (must go before call to autoCapital) */
        if (keyCode == KeyEvent.KEYCODE_POUND) {
            context.textMode.rotate();
            return true;
        }

        /* If not numbers mode, we want to handle this event ourselves still */
        if (context.textMode.mode != TextMode.NUMBERS) {
            if (n >= 0 && n <= 9) /* Follow for long-presses on numbers */
                registerLongPressEventHanlder(keyCode, new OnLongPressEventHandler() {
                    @Override
                    public void onLongPressEvent(int keyCode) {
                        if (kh != null) {
                            kh.cancel();
                            kh.finish();
                        }
                        context.ic.commitText(Integer.toString(Tables.getKCNum(keyCode)), 1);
                    }
                });
            //noinspection ConstantConditions
            if (kh.keyDown(keyCode, context.textMode.autoCapital())) {
                if (keyCode == KeyEvent.KEYCODE_STAR)
                    languages.finish();
                return true;
            }
        }

        /* Handle * key for language switch (if T9Predict didn't eat it) or normal mode */
        if (keyCode == KeyEvent.KEYCODE_STAR) {
            if (context.textMode.isPredictive) {
                languages.nextLanguage();
                return true;
            } else {
                if (kv != null && kv.getWindowToken() != null)
                    new Star(this, context);
                return true;
            }
        }

        if (addingWord) {
            kv.findViewById(R.id.addWordEditor).onKeyDown(keyCode, event);
            return true; /* Don't give anything to other editor if in add word mode */
        }
        return false;
    }

    @Override
    public void mOnUpdateSelection(int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) {
        /* This is only received for normal context */
        KeyHandler kh = contextNormal.getKeyHandler();
        if (kh != null)
            kh.selChanged(newSelStart, newSelEnd, candidatesStart, candidatesEnd);
        if (contextNormal.textMode.mode == TextMode.AUTO)
            contextNormal.textMode.updateView(); /* Autocapital view changes according to cursor position (the coloring) */
    }

    /* Finish up current input method, used on start/end and change mode */
    public void endInput(InputContext context) {
        KeyHandler kh = context.getKeyHandler();
        if (kh != null)
            kh.finish();
    }

    /* This is for the special characters dialog to insert them */
    public void insert(String s) {
        KeyHandler kh = contextCurrent.getKeyHandler();
        if (kh != null)
            kh.finish();
        if (contextCurrent.ic != null)
            contextCurrent.ic.commitText(s, 1);
    }

    /* This is for AddWord shit to set the current word */
    private void setWord(String w) { /* Inserts in normal context regardless of whatever */
        if (contextNormal.getKeyHandler() == contextNormal.predict) {
            boolean[] capitals = new boolean[w.length()];
            contextNormal.ic.setComposingText("", 1); /* To make sure autoCapital works */
            for (int i = 0; i < w.length(); ++i) {
                capitals[i] = (contextNormal.textMode.mode == TextMode.UPPERCASE ||
                               (i == 0 && contextNormal.textMode.autoCapital()) ||
                               Character.isUpperCase(w.charAt(i)));
            }
            contextNormal.predict.setWord(w, capitals);
        } else {
            KeyHandler kh = contextNormal.getKeyHandler();
            if (kh != null)
                kh.finish();
            if (contextNormal.textMode.mode == TextMode.UPPERCASE)
                w = w.toUpperCase();
            if (contextNormal.textMode.autoCapital())
                w = w.substring(0, 1).toUpperCase() + w.substring(1);
            contextNormal.normal.finish();
            contextNormal.ic.commitText(w, 1);
        }
    }

    /* Update all things in our View */
    private void updateView() {
        predictionView.updateView();
        contextCurrent.textMode.updateView();
        status.updateView();
        languages.updateView();
    }

    /*****************************
     * Support stuff for AddWord *
     *****************************/

    public boolean addingWord = false;
    private Status.Item addWordStatus;
    private int addWordMode = 0;
    private Language addWordLang; /* So the display always matches what really happens, even if user changed settings */
    private final static int ADDWORDMODE_LANG = 0;
    private final static int ADDWORDMODE_ALL = 1;
    private final static int ADDWORDMODE_NONE = 2;
    private final static int ADDWORDMODECOUNT = 3;

    private void addWord_start() {
        if (kv == null)
            return;
        /* Create editor and show it off */
        LinearLayout ll = (LinearLayout) kv.findViewById(R.id.predictionsBarContent);
        ll.setVisibility(View.GONE);
        RelativeLayout rl = (RelativeLayout) kv.findViewById(R.id.addWordBarContent);
        rl.setVisibility(View.VISIBLE);
        AddWordEdit addWordInput = (AddWordEdit) kv.findViewById(R.id.addWordEditor);

        if (prefs.addWordCurrent) {
            addWordInput.setText(contextNormal.predict.getWord());
            addWordInput.selectAll();
        } else addWordInput.setText("");

        /* Make the editor visible */
        addWordInput.setVisibility(View.VISIBLE);
        addWordInput.requestFocus();
        /* Start the magic */
        addingWord = true;
        /* Create new state */
        contextCurrent = new InputContext();
        contextCurrent.ic = addWordInput.onCreateInputConnection(new EditorInfo());
        contextCurrent.textMode = new TextMode(this, contextCurrent);
        //predictionView = new PredictionsView(this, prefs);
        contextCurrent.normal = new T9Normal(this, contextCurrent);
        contextCurrent.textMode.setPredictiveEnabled(false); /* Both of these methods will call updateView */
        contextCurrent.textMode.setTextMode(TextMode.LOWERCASE, false, false, contextNormal.textMode.charset);
        addWordMode = 0;
        addWordMode_update();
    }

    private void addWord_end(boolean success) {
        RelativeLayout rl = (RelativeLayout) kv.findViewById(R.id.addWordBarContent);
        rl.setVisibility(View.GONE);
        LinearLayout ll = (LinearLayout) kv.findViewById(R.id.predictionsBarContent);
        ll.setVisibility(View.VISIBLE);
        InputContext context = contextCurrent;
        contextCurrent = contextNormal;
        addingWord = false;
        status.remove(addWordStatus);
        if (success) {
            String word = ((AddWordEdit) kv.findViewById(R.id.addWordEditor)).getText().toString();
            switch(addWordMode) {
                case ADDWORDMODE_LANG:
                    addWordLang.userDictionary.add(context.textMode.charset, word);
                    break;
                case ADDWORDMODE_ALL:
                    languages.userDictionary.add(context.textMode.charset, word);
                    break;
            }
            setWord(word); /* This needs to go after restoring contextCurrent because TextModeautoCapital uses that */
        }
        updateView();
    }

    private void addWordMode_rotate() {
        addWordMode = ((++addWordMode < ADDWORDMODECOUNT) ? addWordMode : 0);
        addWordMode_update();
    }

    private void addWordMode_rotate_cancel() {
        if (!addingWord)
            return;
        addWordMode = ((--addWordMode >= 0) ? addWordMode : (ADDWORDMODECOUNT - 1));
        addWordMode_update();
    }

    private void addWordMode_update() {
        String mode = null;
        switch (addWordMode) {
            case ADDWORDMODE_LANG:
                addWordLang = languages.current;
                mode = String.format(getString(R.string.status_addWordLang), addWordLang.name);
                break;
            case ADDWORDMODE_ALL:
                mode = getString(R.string.status_addWordAll);
                break;
            case ADDWORDMODE_NONE:
                mode = getString(R.string.status_addWordNone);
                break;
        }
        status.remove(addWordStatus);
        addWordStatus = status.add(mode, false);
    }
}
