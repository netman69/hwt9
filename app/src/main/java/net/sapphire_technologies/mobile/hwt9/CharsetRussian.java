package net.sapphire_technologies.mobile.hwt9;

/**
 * Created by netman on 2/04/2017.
 *
 * Russian charset
 */
public class CharsetRussian extends Charset {
    @Override
    public String getName() {
        return "russian";
    }

    @Override
    public String getVisibleName() {
        return "Pусски";
    }

    @Override
    public String getAbc() {
        return "aбв";
    }

    @Override
    public char[] getMap(char n) {
        switch (n) {
            case 1:
                return new char[] { '.', ',', '?', '!', '\'', '"', '1', '-', '(', ')', '@', '/', ':', ';', '_', '*', '|' };
            case 2:
                return new char[] { 'а', 'б', 'в', 'г', '2' };
            case 3:
                return new char[] { 'д', 'е', 'ж', 'з', '3' };
            case 4:
                return new char[] { 'и', 'й', 'к', 'л', '4' };
            case 5:
                return new char[] { 'м', 'н', 'о', 'п', '5' };
            case 6:
                return new char[] { 'р', 'с', 'т', 'у', '6' };
            case 7:
                return new char[] { 'ф', 'х', 'ц', 'ч', '7' };
            case 8:
                return new char[] { 'ш', 'щ', 'ъ', 'ы', '8' };
            case 9:
                return new char[] { 'ь', 'э', 'ю', 'я', '9' };
            case 0:
                return new char[] { ' ', '0' };
        }
        return null;
    }

    @Override
    public char getMapReverse(char c) {
        switch (c) {
            case '.':
            case ',':
            case '?':
            case '!':
            case '\'':
            case '"':
            case '1':
            case '-':
            case '(':
            case ')':
            case '@':
            case '/':
            case ':':
            case ';':
            case '_':
            case '*':
            case '|':
                return 1;
            case 'а':
            case 'б':
            case 'в':
            case 'г':
            case '2':
                return 2;
            case 'д':
            case 'е':
            case 'ж':
            case 'з':
            case '3':
                return 3;
            case 'и':
            case 'й':
            case 'к':
            case 'л':
            case '4':
                return 4;
            case 'м':
            case 'н':
            case 'о':
            case 'п':
            case '5':
                return 5;
            case 'р':
            case 'с':
            case 'т':
            case 'у':
            case '6':
                return 6;
            case 'ф':
            case 'х':
            case 'ц':
            case 'ч':
            case '7':
                return 7;
            case 'ш':
            case 'щ':
            case 'ъ':
            case 'ы':
            case '8':
                return 8;
            case 'ь':
            case 'э':
            case 'ю':
            case 'я':
            case '9':
                return 9;
            case ' ':
            case '0':
                return 0;
        }
        return 10;
    }

    @Override
    public char[] getSpecial() {
        return new char[] { '.', ',', '\'', '?', '!', '\"', '-', '(', ')', '@', '/', ':', '_', ';', '+', '&', '%', '*', '=', '<', '>', '£', '€', '$', '¥', '°', '[', ']', '{', '}', '\\', '~', '^', '¡', '¿', '§', '#', '|', 'μ', '\n' };
    }

    @Override
    public boolean isWordCharacter(char c) {
        int n = getMapReverse(Character.toLowerCase(c));
        return (n > 1 && n < 10 && (c > '9' || c < '0'));
    }

    @Override
    public boolean needCapital(String bc, int textMode) {
        if (bc != null && bc.length() > 2)
            bc = bc.substring(0, 2);
        if (textMode == TextMode.AUTO) {
            if (bc == null || bc.length() == 0 || bc.equals(". ") || bc.equals("! ") || bc.equals("? ") || bc.substring(bc.length() - 1).equals("\n"))
                return true;
        }
        return textMode == TextMode.UPPERCASE;
    }
}
