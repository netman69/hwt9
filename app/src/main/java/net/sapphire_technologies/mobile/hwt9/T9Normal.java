package net.sapphire_technologies.mobile.hwt9;

import android.os.Handler;
import android.view.KeyEvent;

/**
 * Created by netman on 3/18/17.
 *
 * Non-predictive input KeyHandler
 */
public class T9Normal extends KeyHandler {
    private int pos = 0;
    private int keyCode_prev = 0;
    private final Handler timer = new Handler();
    private Runnable timerTask;
    private boolean wasCapitalized = false, finished = true;
    private final HWT9 hwt9;
    private final HWT9.InputContext context;

    public T9Normal(HWT9 hwt9, HWT9.InputContext context) {
        super(hwt9, context);
        this.hwt9 = hwt9;
        this.context = context;
    }

    @Override
    public void finish() {
        killTimer();
        if (context.ic != null)
            context.ic.finishComposingText();
        pos = 0;
        keyCode_prev = 0;
        finished = true;
    }

    @Override
    public void cancel() {
        /* If the timer already finished we're too late to do anything else than delete */
        /* Hence we finish and do backspace equivalent thing */
        finish();
        context.ic.deleteSurroundingText(1, 0);
    }

    @Override
    public boolean keyDown(int keyCode, boolean isCapitalized) {
        char[] map = context.textMode.charset.getMap(Tables.getKCNum(keyCode));
        if (map != null) {
            killTimer();
            if (keyCode == keyCode_prev) {
                ++pos;
            } else finish(); /* This will 0 the position too */

            if (pos >= map.length)
                pos = 0;
            char c = map[pos];
            if (finished)
                wasCapitalized = isCapitalized;
            if (wasCapitalized)
                c = Character.toUpperCase(c);

            context.ic.setComposingText(String.valueOf(c), 1);
            finished = false;

            timerTask = new Runnable() {
                @Override
                public void run() {
                    /* Same as finish without killTimer */
                    //if (ic != null)
                    context.ic.finishComposingText();
                    pos = 0;
                    keyCode_prev = 0;
                    finished = true;
                }
            };
            timer.postDelayed(timerTask, hwt9.prefs.commitDelay);

            keyCode_prev = keyCode;
            return true;
        } else {
            /* Eat the DPAD_RIGHT event if it cancelled the timer */
            if (!finished && keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                finish();
                return true;
            }
            /* Else just pass the unhandled key but kill the timer */
            finish();
        }
        keyCode_prev = keyCode;
        return false;
    }

    @Override
    public void selChanged(int selStart, int selEnd, int cStart, int cEnd) {
        if (!finished && cEnd >= 0 && cEnd != selEnd)
            finish();
    }

    private void killTimer() {
        if (timerTask != null)
            timer.removeCallbacks(timerTask);
        timerTask = null;
    }
}
