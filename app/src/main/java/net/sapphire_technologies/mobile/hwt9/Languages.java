package net.sapphire_technologies.mobile.hwt9;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by netman on 1/04/2017.
 *
 * Languages switching etcetera
 */
public class Languages extends ArrayList<Language> {
    private final HWT9 hwt9;
    public Language current;
    private Language prev;

    public final static String assetsdir = "dictionaries";

    public final ArrayList<Language> addons = new ArrayList<>();
    public final UserDictionary userDictionary;

    /* Receiver to update language and charset selections if they changed in the dialog */
    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            hwt9.charsets.scan();
            scan();
        }
    };

    public final BroadcastReceiver receiverReload = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            hwt9.charsets.scan();
            scan();
            for (Language l : hwt9.languages) {
                l.unLoad();
                l.load();
            }
            for (Language l : hwt9.languages.addons) {
                l.unLoad();
                l.load();
            }
            userDictionary.unLoad();
            userDictionary.load();
        }
    };

    public Languages(final HWT9 hwt9) {
        this.hwt9 = hwt9;
        userDictionary = new UserDictionary(hwt9, "userwords");
        userDictionary.load();
    }

    private void setLanguage(Language language) {
        boolean isChanged = prev != current;
        prev = current;
        current = language;
        hwt9.contextNormal.predict.onLanguageChanged();
        hwt9.contextNormal.textMode.setLanguage(language);
        updateView();
        if (hwt9.kv != null && !hwt9.kv.isShown() && isChanged && hwt9.contextNormal.textMode.isPredictive)
            hwt9.showWindow(true);
    }

    public void nextLanguage() {
        if (current != null) {
            int cl = indexOf(current) + 1;
            if (cl >= size())
                cl = 0;
            setLanguage(get(cl));
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean cancel() {
        if (prev == null)
            return false;
        setLanguage(prev);
        prev = null;
        return true;
    }

    public void finish() {
        prev = null; /* This stops cancel from working (that's the purpose) */
    }

    /* Scan for installed languages and charsets */
    public void scan() {
        /* Do the actual scanning */
        scanStatic(hwt9);

        /* Get a list of all the languages in our SharedPreferences */
        SharedPreferences sp = hwt9.getSharedPreferences("hwt9_languages", Context.MODE_PRIVATE);
        Set<String> keys = sp.getAll().keySet();

        /* Create language object for each of the enabled ones */
        ArrayList<Language> newLanguages = new ArrayList<>();
        ArrayList<Language> newAddons = new ArrayList<>();
        for (String key : keys) {
            if ((key.charAt(0) != 'f' && key.charAt(0) != 'a') || !sp.getBoolean(key, false))
                continue;
            String fileName = key.substring(1);
            String name = sp.getString('n' + fileName, null);
            if (name == null)
                continue;
            if (key.charAt(0) == 'f')
                newLanguages.add(new Language(hwt9, name, fileName, false));
            if (key.charAt(0) == 'a')
                newAddons.add(new Language(hwt9, name, fileName, true));
        }

        /* Load any newly enabled dictionaries */
        for (Language l : newLanguages) {
            if (contains(l)) { /* Check if it's already there, if so, skip it */
                Log.d("HWT9 Languages", "Dictionary \"" + l.name + "\" already present");
                continue;
            } else Log.d("HWT9 Languages", "Adding dictionary \"" + l.name + "\"");
            if (l.load()) /* But only if loading succeeds */
                add(l);
        }
        /* Same for addons */
        for (Language l : newAddons) {
            if (addons.contains(l)) { /* Check if it's already there, if so, skip it */
                Log.d("HWT9 Languages", "Add-on dictionary \"" + l.name + "\" already present");
                continue;
            } else Log.d("HWT9 Languages", "Adding add-on dictionary \"" + l.name + "\"");
            if (l.load())
                addons.add(l);
        }

        /* Remove disappeared/disabled dictionaries */
        ArrayList<Language> remove = new ArrayList<>();
        for (Language l : this)
            if (!newLanguages.contains(l))
                remove.add(l); /* We create list of things to remove, because removing from the list we iterate can get confusing */
        for (Language l : remove) {
            l.unLoad();
            remove(l);
        }
        /* Same for addons */
        remove = new ArrayList<>();
        for (Language l : addons)
            if (!newAddons.contains(l))
                remove.add(l); /* We create list of things to remove, because removing from the list we iterate can get confusing */
        for (Language l : remove) {
            l.unLoad();
            addons.remove(l);
        }

        /* Find if we still have the same language as before */
        boolean ok = false;
        prev = null;
        if (current != null)
            for (Language l : this)
                if (l.name.equals(current.name))
                    ok = true;

        /* If not just set first language we can find */
        if (!ok) {
            current = null;
            if (size() > 0)
                setLanguage(this.get(0));
        }

        /* Make sure the disabled languages are garbage-collected */
        /* Now disabled cause it impacts performance negatively */
        /*Runtime.getRuntime().gc();
        System.gc();*/

        /* If we have no languages, disable predictive text */
        if (size() == 0)
            hwt9.contextNormal.textMode.setPredictiveEnabled(false);
        else hwt9.contextNormal.textMode.setPredictiveEnabled(true);
    }

    /* Scan for installed languages and charsets when no hwt9 is running */
    public static void scanStatic(Context ctx) { /* Synchronized because ran from Star and HWT9 both */
        ArrayList<StaticLang> all = new ArrayList<>();
        File dir = ctx.getExternalFilesDir(null);
        //noinspection ConstantConditions

        HashSet<String> dupcheck = new HashSet<>();
        HashSet<String> dupcheckAddons = new HashSet<>();
        HashSet<String> dupcheckFileName = new HashSet<>();

        File files[] = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (!file.getName().endsWith(".txt"))
                    continue;
                try {
                    dupcheckFileName.add(file.getName());
                    checkDictionaryStatic(new FileInputStream(file), file.getName(), dupcheck, dupcheckAddons, all);
                } catch (Exception e) {
                    /* Intentionally empty */
                }
            }
        }
        try {
            AssetManager am = ctx.getAssets();
            String assets[] = am.list(assetsdir);
            for (String asset : assets) {
                if (dupcheckFileName.contains(asset) || !asset.endsWith(".txt"))
                    continue;
                dupcheckFileName.add(asset);
                checkDictionaryStatic(am.open(assetsdir + "/" + asset), asset, dupcheck, dupcheckAddons, all);
            }
        } catch (Exception e) {
            /* Intentionally empty */
        }

        SharedPreferences sp = ctx.getSharedPreferences("hwt9_languages", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        /* Get a list of all the languages in our SharedPreferences */
        Set<String> keys = sp.getAll().keySet();
        /* Remove all the ones that we found a file for from that list */
        for (StaticLang l : all) {
            ed.putString('n' + l.fileName, l.name); /* Set the name in the preferences thing */
            keys.remove('n' + l.fileName);
            String prefix = (l.isAddon ? "a" : "f");
            if (!keys.remove(prefix + l.fileName)) /* And if there is a dictionary file that has no item we can remove */
                ed.putBoolean(prefix + l.fileName, false); /* Add it to the SharedPreferences as disabled */
        }
        /* Remove all the ones that we did not remove from the list from the SharedPreferences */
        for (String k : keys) {
            if (k.charAt(0) != 'f' && k.charAt(0) != 'a' && k.charAt(0) != 'n')
                continue; /* Only remove f, a and n prefixed things */
            ed.remove(k);
        }
        ed.commit();
    }

    public void stopLoading() {
        for (Language l : this)
            l.unLoad();
        for (Language l : addons)
            l.unLoad();
        userDictionary.unLoad();
    }

    public boolean isWordCharacter(char c) {
        if (hwt9.prefs.combineLanguages) {
            for (Language l : hwt9.languages)
                if (l.charset.isWordCharacter(c))
                    return true;
            return false;
        } else {
            return hwt9.languages.current.charset.isWordCharacter(c);
        }
    }

    public char getMapReverse(char c) {
        if (hwt9.prefs.combineLanguages) {
            char r = hwt9.languages.current.charset.getMapReverse(c);
            if (r != 10)
                return r;
            for (Language l : hwt9.languages) {
                if (l == hwt9.languages.current)
                    continue;
                r = l.charset.getMapReverse(c);
                if (r != 10)
                    return r;
            }
            return 10;
        } else {
            return hwt9.languages.current.charset.getMapReverse(c);
        }
    }

    public void updateView() {
        if (hwt9.kv != null) {
            TextView tv = (TextView) hwt9.kv.findViewById(R.id.language);
            tv.setText((current != null) ? current.name : hwt9.getString(R.string.nolanguage));
        }
    }

    static class StaticLang {
        public String name, fileName;
        boolean isAddon;
        public StaticLang(String n, String fn, boolean ia) {
            name = n;
            fileName = fn;
            isAddon = ia;
        }
    }

    private static void checkDictionaryStatic(InputStream fis, String fileName, HashSet<String> dupcheck, HashSet<String> dupcheckAddons, ArrayList<StaticLang> all) {
        //noinspection MismatchedQueryAndUpdateOfCollection
        INIParser parser = new INIParser(fis, false);
        HashMap<String, String> info = parser.get("dictionary");
        if (info != null) {
            String name = info.get("name");
            if (name != null) {
                    /* Deal with duplicate names */
                if (dupcheck.contains(name)) {
                    Log.d("HWT9 Languages", "Duplicate language name found.");
                    int n = 2;
                    while (dupcheck.contains(name + " " + Integer.toString(n)))
                        ++n;
                    name = name + " " + Integer.toString(n);
                }
                dupcheck.add(name);
                    /* Add this language */
                all.add(new StaticLang(name, fileName, false));
            }
        }
            /* Look for dictionary addons */
        info = parser.get("addondictionary");
        if (info != null) {
            String name = info.get("name");
            if (name != null) {
                    /* Deal with duplicate names */
                if (dupcheckAddons.contains(name)) {
                    Log.d("HWT9 Languages", "Duplicate addon dictionary name found.");
                    int n = 2;
                    while (dupcheckAddons.contains(name + " " + Integer.toString(n)))
                        ++n;
                    name = name + " " + Integer.toString(n);
                }
                dupcheckAddons.add(name);
                    /* Add this language */
                all.add(new StaticLang(name, fileName, true));
            }
        }
    }
}
