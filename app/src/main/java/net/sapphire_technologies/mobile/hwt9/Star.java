package net.sapphire_technologies.mobile.hwt9;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.IBinder;
import android.text.Html;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by netman on 23/03/2017.
 *
 * Special characters menu
 */
@SuppressWarnings("deprecation")
class Star {
    private AlertDialog dlg;
    private Button[] buttons;
    private final HWT9 hwt9;
    private final HWT9.InputContext context;

    /* This is the class handling the menu stuff when * button pressed */
    @SuppressLint("InflateParams")
    public Star(final HWT9 hwt9, HWT9.InputContext context) {
        this.hwt9 = hwt9;
        this.context = context;
        /* Try to get the window token */
        IBinder token = hwt9.kv.getWindowToken();
        if (token == null) { /* Open the window if it isn't shown */
            hwt9.showWindow(true); /* We kinda sorta need the window to get it's window token */
            token = hwt9.kv.getWindowToken();
        }
        if (token == null)
            return;
        /* Mess to be able to create a dialog */
        AlertDialog.Builder builder;
        /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = new AlertDialog.Builder(hwt9, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        } else */ builder = new AlertDialog.Builder(new ContextThemeWrapper(hwt9, R.style.StarDialogStyle));
        builder.setTitle(hwt9.getString(R.string.app_name));
        View view = hwt9.getLayoutInflater().inflate(R.layout.star, null);
        builder.setView(view);
        dlg = builder.create();
        Window window = dlg.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.token = token;
        lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG;
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        /* Now make use of it */
        Button b;
        b = (Button) view.findViewById(R.id.star_star);
        setButtonText(b, hwt9.getString(R.string.next), "*");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { nextPage(); }
        });
        b = (Button) view.findViewById(R.id.star_pound);
        setButtonText(b, hwt9.getString(R.string.prev), "#");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { prevPage(); }
        });
        buttons = new Button[] {
            (Button) view.findViewById(R.id.star_1),
            (Button) view.findViewById(R.id.star_2),
            (Button) view.findViewById(R.id.star_3),
            (Button) view.findViewById(R.id.star_4),
            (Button) view.findViewById(R.id.star_5),
            (Button) view.findViewById(R.id.star_6),
            (Button) view.findViewById(R.id.star_7),
            (Button) view.findViewById(R.id.star_8),
            (Button) view.findViewById(R.id.star_9),
            (Button) view.findViewById(R.id.star_0),
        };
        for (Button button : buttons) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < buttons.length; ++i) {
                        if (buttons[i] == v) {
                            onSpecialKeyPress(i);
                            break;
                        }
                    }
                }
            });
        }
        updatePage(context.textMode.charset.getSpecial());
        dlg.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getRepeatCount() != 0 || event.getAction() != KeyEvent.ACTION_DOWN || event.isLongPress())
                    return false;
                switch (keyCode) {
                    case KeyEvent.KEYCODE_1:
                        onSpecialKeyPress(0);
                        return true;
                    case KeyEvent.KEYCODE_2:
                        onSpecialKeyPress(1);
                        return true;
                    case KeyEvent.KEYCODE_3:
                        onSpecialKeyPress(2);
                        return true;
                    case KeyEvent.KEYCODE_4:
                        onSpecialKeyPress(3);
                        return true;
                    case KeyEvent.KEYCODE_5:
                        onSpecialKeyPress(4);
                        return true;
                    case KeyEvent.KEYCODE_6:
                        onSpecialKeyPress(5);
                        return true;
                    case KeyEvent.KEYCODE_7:
                        onSpecialKeyPress(6);
                        return true;
                    case KeyEvent.KEYCODE_8:
                        onSpecialKeyPress(7);
                        return true;
                    case KeyEvent.KEYCODE_9:
                        onSpecialKeyPress(8);
                        return true;
                    case KeyEvent.KEYCODE_0:
                        onSpecialKeyPress(9);
                        return true;
                    case KeyEvent.KEYCODE_STAR:
                        nextPage();
                        return true;
                    case KeyEvent.KEYCODE_POUND:
                        prevPage();
                        return true;
                }
                return false;
            }
        });
        b = (Button) view.findViewById(R.id.star_settings);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(hwt9, PreferencesDialog.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                hwt9.startActivity(intent);
                dlg.dismiss();
            }
        });
        b = (Button) view.findViewById(R.id.star_language);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Languages.scanStatic(hwt9); /* Look for new languages before showing the dialog */
                Intent i = new Intent(hwt9, LanguagesDialog.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                hwt9.startActivity(i);
                dlg.dismiss();
            }
        });
        /* Show ourselves */
        dlg.show();
    }

    private int page = 0;

    private void nextPage() {
        int off = buttons.length * ++page;
        char[] special = context.textMode.charset.getSpecial();
        if (off >= special.length)
            page = 0;
        updatePage(special);
    }

    private void prevPage() {
        int off = buttons.length * --page;
        char[] special = context.textMode.charset.getSpecial();
        if (off <= 0)
            page = 0;
        updatePage(special);
    }

    private void updatePage(char[] special) {
        int off = buttons.length * page;
        dlg.setTitle(hwt9.getString(R.string.page) + " " + Integer.toString(page + 1) + "/" + Integer.toString(special.length / buttons.length));
        for (int i = 0; i < buttons.length; ++i) {
            int n = (1 + i) % 10;
            if (off + i >= special.length) {
                buttons[i].setEnabled(false);
                setButtonText(buttons[i], Integer.toString(n), Integer.toString(n));
                continue;
            }
            buttons[i].setEnabled(true);
            setButtonText(buttons[i], String.valueOf(special[off + i]), Integer.toString(n));
        }
    }

    private void setButtonText(Button b, String text, String buttonName) {
        text = text.replace('\n', '↵'); /* Make newline visible */
        text = TextUtils.htmlEncode(text);
        b.setText(Html.fromHtml(String.format("%s<font color='" + hwt9.getResources().getColor(R.color.button_text2) + "'><sup>%s</sup></font>", text, buttonName)));
    }

    private void onSpecialKeyPress(int i) { /* Buttons are indexed 1 2 3 ... 7 8 9 0 */
        int off = buttons.length * page;
        char[] special = context.textMode.charset.getSpecial();
        hwt9.insert(String.valueOf(special[off + i]));
        if (hwt9.prefs.starDismissOnChar)
            dlg.dismiss();
    }
}
