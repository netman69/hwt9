package net.sapphire_technologies.mobile.hwt9;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by netman on 2/04/2017.
 *
 * INI file parser, supports special [data] section, where parser stops reading and allows
 *   getLine() to get lines of arbitrary data stripped from comments.
 * Or the caller can read from the InputStream after that.
 * If wantData argument for constructor is true only, and data section is present hasData will be
 *   true, and inputStream left open. Otherwise inputStream will be closed.
 * If hasData == true, .end() can be used to close the input stream conveniently.
 * Note that keys and sections are made to lowercase automatically, but values aren't.
 */
class INIParser extends HashMap<String, HashMap<String, String>> {
    private final BufferedReader bufferedReader;
    private HashMap<String, String> section;
    public boolean hasData = false;
    private static final int maxLines = 1024;

    public INIParser(InputStream inputStream, boolean wantData) {
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        Log.d("INIParser", "File start.");
        int n = 0;
        while ((line = getLine()) != null && ++n < maxLines) {
            if (line.length() == 0)
                continue;
            if (line.charAt(0) == '[' && line.charAt(line.length() - 1) == ']') {
                String sectionName = line.substring(1, line.length() - 1).trim().toLowerCase();
                section = new HashMap<>();
                put(sectionName, section);
                Log.d("INIParser", "Section start: \"" + sectionName + "\"");
                if (sectionName.equals("data")) {
                    hasData = true;
                    Log.d("INIParser", "Got data.");
                    break;
                }
                continue;
            }
            int idx = line.indexOf('=');
            if (idx == -1)
                continue;
            String key = line.substring(0, idx).trim().toLowerCase();
            String val = line.substring(idx + 1, line.length()).trim();
            section.put(key, val);
            Log.d("INIParser", "New pair: \"" + key + "\"=\"" + val + "\"");
        }
        if (n == maxLines)
            Log.d("INIParser", "Too much text!");
        if (!wantData)
            end();
    }

    public String getLine() {
        try {
            String line = bufferedReader.readLine();
            if (line == null)
                return null;
            return stripComments(line);
        } catch (IOException e) {
            return null;
        }
    }

    public void end() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            /* Intentionally empty */
        }
    }

    private String stripComments(String s) {
        int idx = s.indexOf('#');
        if (idx != -1)
            s = s.substring(0, idx);
        return s.trim();
    }
}
