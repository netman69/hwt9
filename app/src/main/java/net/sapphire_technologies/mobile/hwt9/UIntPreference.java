package net.sapphire_technologies.mobile.hwt9;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

/**
 * Created by netman on 10/04/2017.
 *
 * Preference to ask for numbers
 */
public class UIntPreference extends EditTextPreference {
    private int number;

    public UIntPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initEdit();
    }

    public UIntPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        initEdit();
    }

    public UIntPreference(Context context) {
        super(context);
        initEdit();
    }

    @Override
    public void showDialog(Bundle state) {
        super.showDialog(state);
        getEditText().selectAll();
    }

    @Override
    public void setText(String text) {
        final boolean wasBlocking = shouldDisableDependents();
        setNumber(text);
        persistString(Integer.toString(number));
        final boolean isBlocking = shouldDisableDependents();
        if (isBlocking != wasBlocking) notifyDependencyChange(isBlocking);
    }

    @Override
    public String getText() {
        return Integer.toString(number);
    }

    @Override
    public CharSequence getSummary() {
        String text = getText();
        if (TextUtils.isEmpty(text)) {
            CharSequence hint = getEditText().getHint();
            if (!TextUtils.isEmpty(hint)) {
                return hint;
            } else {
                return super.getSummary();
            }
        } else {
            CharSequence summary = super.getSummary();
            if (!TextUtils.isEmpty(summary)) {
                return String.format(summary.toString(), text);
            } else {
                return summary;
            }
        }
    }

    private void initEdit() {
        EditText edit = getEditText();
        edit.setInputType(InputType.TYPE_CLASS_NUMBER);
        edit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && Keys.getAbstractedKey(keyCode) == Keys.KEY_OK) {
                    ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    return true;
                }
                return false;
            }
        });
    }

    private void setNumber(String text) {
        try {
            int n = Integer.valueOf(text);
            if (n < 0)
                n = 0;
            number = n;
        } catch (Exception e) {
            /* Intentionally empty */
        }
    }
}
