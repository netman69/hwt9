package net.sapphire_technologies.mobile.hwt9;

import android.os.Process;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

/**
 * Created by netman on 6/04/2017.
 *
 * For keeping words added by the user
 */
public class UserDictionary {
    private final HWT9 hwt9;
    private final String name;
    public Trie trie;
    private boolean enabled = false;
    private Thread thread;

    public UserDictionary(HWT9 hwt9, @SuppressWarnings("SameParameterValue") String name) {
        this.hwt9 = hwt9;
        this.name = name;
        trie = new Trie(hwt9.prefs);
    }

    public void load() {
        final Status.Item it = hwt9.status.add(hwt9.getString(R.string.status_loadingDictionary), true);
        enabled = true;
        thread = new Thread() {
            @Override
            public void run() {
                Charset[] sets = Charsets.getAll();
                for (Charset charset : sets) {
                    try {
                        //noinspection ConstantConditions
                        File file = new File(getFileName(charset));
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            trie.add(charset, line, true);
                            synchronized (thread) {
                                if (!enabled)
                                    break;
                            }
                        }
                        reader.close();
                    } catch (Exception e) {
                        /* Intentionally emtpy */
                    }
                }
                hwt9.status.remove(it);
            }
        };
        thread.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
    }

    public void unLoad() {
        if (thread != null) {
            synchronized (thread) {
                enabled = false;
            }
        }
        trie = new Trie(hwt9.prefs);
    }

    public void add(Charset charset, String str) {
        FileOutputStream fos;
        trie.add(charset, str, true); /* Trie automatically takes away duplicates if front==true */
        try {
            String file = getFileName(charset);
            fos = new FileOutputStream(file, true);
            try {
                FileWriter fw = new FileWriter(fos.getFD());
                fw.append(str + "\n");
                fw.close();
            } catch (Exception e) {
                hwt9.status.addTimed(e.getMessage(), false);
                Log.d("HWT9 UserDictionary", e.getMessage());
            } finally {
                fos.getFD().sync();
                fos.close();
            }
        } catch (Exception e) {
            hwt9.status.addTimed(e.getMessage(), false);
            Log.d("HWT9 UserDictionary", e.getMessage());
        }
    }

    private String getFileName(Charset cs) {
        return hwt9.getExternalFilesDir(null).getAbsolutePath() + "/" + name + ".user_" + cs.getName();
    }
}
